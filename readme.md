# SolarStats

Copyright &copy; 2020-2022 by Robert M Cash

## What Does This Thing Do?

SolarStats is meant to solve a relatively simple (ha ha!) problem: laziness. 

I have solar panels installed on my roof (shout out to [Sigora Solar](http://www.sigorasolar.com)). DC power is
converted to AC power by microinverters from [Enphase Energy](http://www.enphase.com/) and information about system
configuration, energy production, etc. is transmitted to Enphase by the Envoy, which is a communications gateway.
The Envoy also captures data about energy consumption and transmits this to Enphase as well.

Enphase offers a free monitoring solution, available via a mobile app and a web application where you can see data
about your system, consumption, cost, etc. You can even run reports. All in all, it's a great system.  So what's not to
like? I have a spreadsheet in Google Docs that I used to track energy production and consumption, to track how much I'm
spending on electricity, and to determine how much money I'm saving because of switching to solar energy. The problem is
how to get the data from Enphase and into my Google spreadsheet. I could easily open up Enphase app every night, look up
the production data, and enter it into my spreadsheet. It would take maybe minutes, but recall the real problem is
laziness. I want this data transfer to happen automatically, without any action on my part, and that's exactly why
SolarStats was created.

## SolarStats 1.0

SolarStats 1.0 ran as a single AWS Lambda function. It was triggered by a scheduled CloudWatch event once a day. When
it ran, it would call Enphase APIs to retrieve data and then send that to an applet on IFTTT which updates my
spreadsheet, stored in Google Drive.

SolarStats 1.0 was good enough, but it had a few pitfalls.
1. It would only ever support my system, my IFTTT applet, etc., as user-specific data was configured at an application
level
2. It had no concept of a customer
3. It was written in 2020 and since that time frameworks it uses have been updated
4. Enphase announced in March 2022 that the version of their API that was being used would be shut down in August.

## SolarStats 2.0

The solution to these problems was to create a new version of SolarStats. It is still triggered by a scheduled event
(AWS has renamed the event portion of CloudWatch as EventBridge), but rather than only supporting 1 consumer, an 
internal API call is made to get information about SolarStats customers (so far I'm the only one). For each customer,
the Lambda function calls Enphase to get production statistics, but rather than publishing to IFTTT, it creates an
event and pushes that to EventBridge. Another Lambda function then pulls events from EventBridge and publishes them to
IFTTT. By splitting the application into two parts, greater throughput has been enabled. In the future, there could
be different event publishers listening for events, should the data need to go to targets other than IFTTT. As well, by
externalizing customer data to a database fronted by an API, support for multiple customers has been added. 

## Tech Stack

* SpringBoot 4.2.x
* Spring Framework 5.2.x
* Spring Cloud 3.2.x

## Tech Details

* **Operating Environment:** Amazon Web Services (AWS) Lambda
* **Trigger:** AWS CloudWatch scheduled event
* **Publisher:** IFTTT Maker template

## Project Info

* **Source repository:** https://bitbucket.org/digithead1011/myenlighten-stats-publisher
