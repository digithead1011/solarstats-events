/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event;

/**
 * Result of a request to publish an event.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class PublishResult
{
	/** Success indicator. */
	private boolean successful = false;

	/** Event id. */
	private String eventId;

	/** Error code. */
	private String errorCode;

	/** Error message. */
	private String errorMessage;

	/** Exception. */
	private Throwable exception;

	/**
	 * Return an instance builder.
	 * 
	 * @return Instance builder.
	 */
	public static Builder builder()
	{
		return new Builder();
	}

	/**
	 * Create new instance of PublishResult.
	 */
	protected PublishResult()
	{
		super();
	}

	/**
	 * Get the success indicator.
	 *
	 * @return Success indicator.
	 */
	public boolean isSuccessful()
	{
		return this.successful;
	}

	/**
	 * Set the success indicator.
	 *
	 * @param argSuccessful Success indicator.
	 */
	protected void setSuccessful(final boolean argSuccessful)
	{
		this.successful = argSuccessful;
	}

	/**
	 * Get the event ID.
	 *
	 * @return Event ID.
	 */
	public String getEventId()
	{
		return this.eventId;
	}

	/**
	 * Set the event ID.
	 *
	 * @param argEventId Event ID.
	 */
	protected void setEventId(final String argEventId)
	{
		this.eventId = argEventId;
	}

	/**
	 * Get the error code.
	 *
	 * @return Error code.
	 */
	public String getErrorCode()
	{
		return this.errorCode;
	}

	/**
	 * Set the error code.
	 *
	 * @param argErrorCode Error code.
	 */
	protected void setErrorCode(final String argErrorCode)
	{
		this.errorCode = argErrorCode;
	}

	/**
	 * Get the error message.
	 *
	 * @return Error message.
	 */
	public String getErrorMessage()
	{
		return this.errorMessage;
	}

	/**
	 * Set the error message.
	 *
	 * @param argErrorMessage Error message.
	 */
	protected void setErrorMessage(final String argErrorMessage)
	{
		this.errorMessage = argErrorMessage;
	}

	/**
	 * Get the exception.
	 *
	 * @return Exception.
	 */
	public Throwable getException()
	{
		return this.exception;
	}

	/**
	 * Set the exception.
	 *
	 * @param argException Exception.
	 */
	protected void setException(final Throwable argException)
	{
		this.exception = argException;
	}

	/**
	 * Instance builder.
	 *
	 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
	 */
	public static class Builder
	{
		/** Publish result being constructed. */
		private final PublishResult result;

		/**
		 * Create a new instance of Builder.
		 */
		protected Builder()
		{
			this.result = new PublishResult();
		}

		/**
		 * Set the success indicator.
		 * 
		 * @param argSuccessful Success indicator.
		 * @return Builder.
		 */
		public Builder withSuccess(final boolean argSuccessful)
		{
			this.result.setSuccessful(argSuccessful);

			return this;
		}

		/**
		 * Set the event ID.
		 * 
		 * @param argEventId Event ID.
		 * @return Builder.
		 */
		public Builder withEventId(final String argEventId)
		{
			this.result.setEventId(argEventId);

			return this;
		}

		/**
		 * Set the error code.
		 * 
		 * @param argErrorCode Error code.
		 * @return Builder.
		 */
		public Builder withErrorCode(final String argErrorCode)
		{
			this.result.setErrorCode(argErrorCode);

			return this;
		}

		/**
		 * Set the error message.
		 * 
		 * @param argErrorMessage Error message.
		 * @return Builder.
		 */
		public Builder withErrorMessage(final String argErrorMessage)
		{
			this.result.setErrorMessage(argErrorMessage);

			return this;
		}

		/**
		 * Set the exception.
		 * 
		 * @param argException Exception.
		 * @return Builder.
		 */
		public Builder withException(final Throwable argException)
		{
			this.result.setException(argException);

			return this;
		}

		/**
		 * Finish constructing the new ProcessingEventPublishResult.
		 * 
		 * @return Processing event publishing result.
		 */
		public PublishResult build()
		{
			return this.result;
		}

	}

}
