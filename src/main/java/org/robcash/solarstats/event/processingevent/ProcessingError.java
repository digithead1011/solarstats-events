/*
 * Copyright (C) 2023-2024 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import jakarta.validation.constraints.NotNull;

/**
 * Processing error.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@JsonPropertyOrder({ "severity", "message" })
@JsonDeserialize(builder = ProcessingError.Builder.class)
public class ProcessingError
{
	/** Severity. */
	private Severity severity;

	/** Message. */
	private String message;

	/** Triggering event. */
	private String triggeringEvent;

	/**
	 * Create a new builder that can construct a new ProcessingError.
	 * 
	 * @return ProcessingError builder.
	 */
	public static Builder builder()
	{
		return new Builder();
	}

	/**
	 * Create a new proesssing error with a severity of Warning.
	 * 
	 * @return Processing error with a severity of Warning.
	 */
	public static ProcessingError newWarning()
	{
		return builder()
				.withSeverity(Severity.Warning)
				.build();
	}

	/**
	 * Create a new proesssing error with a severity of Warning.
	 * 
	 * @param argMessage Error message.
	 * @return Processing error with a severity of Warning.
	 */
	public static ProcessingError newWarning(final String argMessage)
	{
		return builder()
				.withSeverity(Severity.Warning)
				.withMessage(argMessage)
				.build();
	}

	/**
	 * Create a new proesssing error with a severity of Error.
	 * 
	 * @return Processing error with a severity of Error.
	 */
	public static ProcessingError newError()
	{
		return builder()
				.withSeverity(Severity.Error)
				.build();
	}

	/**
	 * Create a new proesssing error with a severity of Error.
	 * 
	 * @param argMessage Error message.
	 * @return Processing error with a severity of Error.
	 */
	public static ProcessingError newError(final String argMessage)
	{
		return builder()
				.withSeverity(Severity.Error)
				.withMessage(argMessage)
				.build();
	}

	/**
	 * Create a new proesssing error with a severity of Fatal.
	 * 
	 * @return Processing error with a severity of Fatal.
	 */
	public static ProcessingError newFatal()
	{
		return builder()
				.withSeverity(Severity.Fatal)
				.build();
	}

	/**
	 * Create a new proesssing error with a severity of Fatal.
	 * 
	 * @param argMessage Error message.
	 * @return Processing error with a severity of Fatal.
	 */
	public static ProcessingError newFatal(final String argMessage)
	{
		return builder()
				.withSeverity(Severity.Fatal)
				.withMessage(argMessage)
				.build();
	}

	/**
	 * Create new instance of ProcessingMessage.
	 */
	protected ProcessingError()
	{
		super();
	}

	/**
	 * Get the severity.
	 *
	 * @return Severity.
	 */
	@JsonProperty
	@NotNull(message = "{validation.processingevent.error.severity.required}")
	public Severity getSeverity()
	{
		return this.severity;
	}

	/**
	 * Set the severity.
	 *
	 * @param argSeverity Severity.
	 */
	protected void setSeverity(final Severity argSeverity)
	{
		this.severity = argSeverity;
	}

	/**
	 * Get the message.
	 *
	 * @return Message.
	 */
	@JsonProperty
	@NotNull(message = "{validation.processingevent.error.message.required}")
	public String getMessage()
	{
		return this.message;
	}

	/**
	 * Get the message.
	 *
	 * @param argMessage Message.
	 */
	protected void setMessage(final String argMessage)
	{
		this.message = argMessage;
	}

	/**
	 * Get the triggering event as JSON.
	 *
	 * @return Triggering event.
	 */
	@JsonProperty
	public String getTriggeringEvent()
	{
		return this.triggeringEvent;
	}

	/**
	 * Set the triggering event as JSON.
	 *
	 * @param argTriggeringEvent Triggering event.
	 */
	protected void setTriggeringEvent(final String argTriggeringEvent)
	{
		this.triggeringEvent = argTriggeringEvent;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode()
	{
		return Objects.hash(this.message, this.severity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final ProcessingError other = (ProcessingError) obj;
		return Objects.equals(this.message, other.message) && this.severity == other.severity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString()
	{
		return "ProcessingError[severity=" + this.severity.getValue() + "; message=" + this.message + "]";
	}

	/**
	 * Severity of message.
	 *
	 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
	 */
	public enum Severity
	{
		/** Warning. */
		Warning("W"),

		/** Error. */
		Error("E"),

		/** Fatal error. */
		Fatal("F");

		/** Internal value. */
		private String value;

		/**
		 * Create new severity enumeration.
		 *
		 * @param argValue Internal value.
		 */
		private Severity(final String argValue)
		{
			this.value = argValue;
		}

		/**
		 * Get the value of this event type.
		 *
		 * @return Value of this event type.
		 */
		@JsonValue
		public String getValue()
		{
			return this.value;
		}

		/**
		 * Get the value of this event type.
		 *
		 * @return Value of this event type.
		 */
		@Override
		public String toString()
		{
			return String.valueOf(this.value);
		}

		/**
		 * Get the enumeration that corresponds to the specified value.
		 *
		 * @param argValue Value used to look up an event type.
		 * @return Event type enumeration or {@code null} no enumeration has the specified value.
		 */
		@JsonCreator
		public static Severity fromValue(final String argValue)
		{
			for (final Severity s : Severity.values())
			{
				if (String.valueOf(s.value).equals(argValue))
				{
					return s;
				}
			}
			return null;
		}

	}

	/**
	 * Builder for instances of Pr ocessingError.
	 *
	 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
	 */
	public static class Builder
	{
		/** Processing error, */
		private final ProcessingError error;

		/**
		 * Create new instance of Builder.
		 */
		protected Builder()
		{
			this.error = new ProcessingError();
		}

		/**
		 * Set the severity.
		 *
		 * @param argSeverity Severity.
		 * @return Processing Error.
		 */
		public Builder withSeverity(final Severity argSeverity)
		{
			this.error.setSeverity(argSeverity);

			return this;
		}

		/**
		 * Set the message.
		 *
		 * @param argMessage Message.
		 * @return Processing Error.
		 */
		public Builder withMessage(final String argMessage)
		{
			this.error.setMessage(argMessage);

			return this;
		}

		/**
		 * Set the triggering event that was being processed when the error occurred.
		 *
		 * @param argTriggeringEvent Triggering event.
		 * @return Processing Error.
		 */
		public Builder withTriggeringEvent(final String argTriggeringEvent)
		{
			this.error.setTriggeringEvent(argTriggeringEvent);

			return this;
		}

		/**
		 * Build the processing error.
		 * 
		 * @return Processing error.
		 */
		public ProcessingError build()
		{
			return this.error;
		}
	}
}
