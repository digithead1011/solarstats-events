/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent;

import java.util.Map;
import java.util.UUID;

import org.robcash.solarstats.event.PublishResult;
import org.robcash.utils.tracing.annotation.Traced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import reactor.core.publisher.Mono;

/**
 * No-op async publisher that swallows publishing events without doing anything.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class NoopProcessingEventAsyncPublisher extends ProcessingEventAsyncPublisher
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(NoopProcessingEventAsyncPublisher.class);

	/**
	 * Create new instance of EventBridgeProcessingEventPublisher.
	 */
	public NoopProcessingEventAsyncPublisher()
	{
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Traced("Publish ProcessingEvent")
	public Mono<PublishResult> publishEvent(final ProcessingEvent argEvent,
			final Map<String, Object> argMetadata)
	{
		Assert.notNull(argEvent, "The event to publish cannot be null");

		LOG.info("No-op event publisher will skip publishing ProcessingEvent to EventBridge");

		final PublishResult result = PublishResult.builder()
				.withSuccess(true)
				.withEventId("noop-" + UUID.randomUUID().toString())
				.build();
		return Mono.just(result);
	}

}
