/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent.config;

import org.robcash.solarstats.event.config.EventsConfigProperties;
import org.robcash.solarstats.event.processingevent.ProcessingEventAsyncPublisher;
import org.robcash.solarstats.event.processingevent.NoopProcessingEventAsyncPublisher;
import org.robcash.solarstats.event.processingevent.NoopProcessingEventPublisher;
import org.robcash.solarstats.event.processingevent.ProcessingEventPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import software.amazon.awssdk.services.eventbridge.EventBridgeAsyncClient;
import software.amazon.awssdk.services.eventbridge.EventBridgeClient;

/**
 * Spring beans for publishing ProcessingEvent events.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(EventsConfigProperties.class)
@ConditionalOnProperty(prefix = "solarstats.events.processing-events", name = "event-source")
public class ProcessingEventsConfig
{
	@Autowired
	private EventsConfigProperties config;

	@Bean
	@ConditionalOnMissingBean
	EventBridgeClient eventBridgeClient()
	{
		return EventBridgeClient.create();
	}

	@Bean
	@ConditionalOnMissingBean
	EventBridgeAsyncClient eventBridgeAsyncClient()
	{
		return EventBridgeAsyncClient.create();
	}

	@Bean
	@ConditionalOnMissingBean
	ProcessingEventPublisher processingEventPublisher(final EventBridgeClient argClient)
	{
		if (this.config.isEnabled())
		{
			return new ProcessingEventPublisher(argClient, this.config.getProcessingEvents());
		}
		else
		{
			return new NoopProcessingEventPublisher();
		}
	}

	@Bean
	@ConditionalOnMissingBean
	ProcessingEventAsyncPublisher asyncProcessingEventPublisher(final EventBridgeAsyncClient argClient)
	{
		if (this.config.isEnabled())
		{
			return new ProcessingEventAsyncPublisher(argClient, this.config.getProcessingEvents());
		}
		else
		{
			return new NoopProcessingEventAsyncPublisher();
		}
	}

}
