/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent;

import java.util.Map;
import java.util.UUID;

import org.robcash.solarstats.event.PublishResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * No-op event processor that does swallows ProcessingEvent events without doing anything.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class NoopProcessingEventPublisher extends ProcessingEventPublisher
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(NoopProcessingEventPublisher.class);

	/**
	 * Create new instance of NoopProcessingEventPublisher.
	 */
	public NoopProcessingEventPublisher()
	{
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublishResult publishEvent(final ProcessingEvent argEvent, final Map<String, Object> argMetadata)
	{
		Assert.notNull(argEvent, "The event to publish cannot be null");

		LOG.info("No-op event publisher will skip publishing ProcessingEvent to EventBridge");

		if (LOG.isDebugEnabled())
		{
			try
			{
				LOG.debug(this.mapper.writeValueAsString(argEvent));
			}
			catch (final JsonProcessingException ex)
			{
				LOG.warn("Failed to log ProcessingEvent as a String", ex);
			}
		}

		return PublishResult.builder()
				.withSuccess(true)
				.withEventId("noop-" + UUID.randomUUID().toString())
				.build();
	}

}
