/*
 * Copyright (C) 2023-2024 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

/**
 * SolarStats processing event.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@JsonPropertyOrder({ "eventName", "version", "id", "correlationId", "action", "phase", "timestamp", "errorDetail",
		"customerId", "metadata" })
@JsonDeserialize(builder = ProcessingEvent.Builder.class)
public class ProcessingEvent implements Serializable
{
	/** Serialization ID. */
	private static final long serialVersionUID = 1L;

	/** Event. */
	private static final String EVENT = "SolarStatsProcessingEvent";

	/** Version. */
	public static final String VERSION = "1";

	/** Version. */
	private String version = VERSION;

	/** ID. */
	private UUID id;

	/** Correlation ID. */
	private String correlationId;

	/** Processing phase. */
	private ProcessingPhase phase;

	/** Action. */
	private Action action;

	/** Timestamp. */
	private Date timestamp;

	/** Error. */
	private ProcessingError errorDetails;

	/** Customer ID. */
	private String customerId;

	/** Metadata. */
	private Map<String, Object> metadata;

	/**
	 * Create a new builder that can construct a new ProcessingEvent.
	 * 
	 * @return ProcessingEvent builder.
	 */
	public static Builder builder()
	{
		return new Builder();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the customer retrieval is starting.
	 * 
	 * @return Processing event.
	 */
	public static ProcessingEvent newCustomerRetrievalStartedEvent()
	{
		return builder()
				.withPhase(ProcessingPhase.CustomerRetrieval)
				.withAction(Action.Started)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the customer retrieval is starting.
	 * 
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newCustomerRetrievalStartedEvent(final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.CustomerRetrieval)
				.withAction(Action.Started)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the customer retrieval has finished.
	 * 
	 * @return Processing event.
	 */
	public static ProcessingEvent newCustomerRetrievalFinishedEvent()
	{
		return builder()
				.withPhase(ProcessingPhase.CustomerRetrieval)
				.withAction(Action.Finished)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the customer retrieval has finished.
	 * 
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newCustomerRetrievalFinishedEvent(final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.CustomerRetrieval)
				.withAction(Action.Finished)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the customer retrieval has encountered an error.
	 * 
	 * @param argError Processing error.
	 * @return Processing event.
	 */
	public static ProcessingEvent newCustomerRetrievalError(final ProcessingError argError)
	{
		return builder()
				.withPhase(ProcessingPhase.CustomerRetrieval)
				.withAction(Action.ErrorOccurred)
				.withErrorDetails(argError)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the customer retrieval has encountered an error.
	 * 
	 * @param argError Processing error.
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newCustomerRetrievalError(final ProcessingError argError,
			final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.CustomerRetrieval)
				.withAction(Action.ErrorOccurred)
				.withErrorDetails(argError)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats retrieval has been requested for a specific
	 * customer.
	 * 
	 * @param argCustomerId Customer ID.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalRequestedEvent(final String argCustomerId)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.Requested)
				.withCustomerId(argCustomerId)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats retrieval has been requested for a specific
	 * customer.
	 * 
	 * @param argCustomerId Customer ID.
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalRequestedEvent(final String argCustomerId,
			final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.Requested)
				.withCustomerId(argCustomerId)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats retrieval is starting.
	 * 
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalStartedEvent()
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.Started)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats retrieval is starting.
	 * 
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalStartedEvent(final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.Started)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats retrieval has started for a specific customer.
	 * 
	 * @param argCustomerId Customer ID.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalStartedEvent(final String argCustomerId)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.Started)
				.withCustomerId(argCustomerId)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats retrieval has started for a specific customer.
	 * 
	 * @param argCustomerId Customer ID.
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalStartedEvent(final String argCustomerId,
			final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.Started)
				.withCustomerId(argCustomerId)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats retrieval has finished.
	 * 
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalFinishedEvent()
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.Finished)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats retrieval has finished.
	 * 
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalFinishedEvent(final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.Finished)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats retrieval has finished for a specific
	 * customer.
	 * 
	 * @param argCustomerId Customer ID.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalFinishedEvent(final String argCustomerId)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.Finished)
				.withCustomerId(argCustomerId)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats retrieval has finished for a specific
	 * customer.
	 * 
	 * @param argCustomerId Customer ID.
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalFinishedEvent(final String argCustomerId,
			final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.Finished)
				.withCustomerId(argCustomerId)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that an error occurred during stats retrieval.
	 * 
	 * @param argError Processing error.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalError(final ProcessingError argError)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.ErrorOccurred)
				.withErrorDetails(argError)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that an error occurred during stats retrieval.
	 * 
	 * @param argError Processing error.
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalError(final ProcessingError argError,
			final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.ErrorOccurred)
				.withErrorDetails(argError)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that an error occurred during stats retrieval.
	 * 
	 * @param argError Processing error.
	 * @param argCustomerId Customer ID.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalError(final ProcessingError argError, final String argCustomerId)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.ErrorOccurred)
				.withErrorDetails(argError)
				.withCustomerId(argCustomerId)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that an error occurred during stats retrieval.
	 * 
	 * @param argError Processing error.
	 * @param argCustomerId Customer ID.
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsRetrievalError(final ProcessingError argError, final String argCustomerId,
			final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withAction(Action.ErrorOccurred)
				.withErrorDetails(argError)
				.withCustomerId(argCustomerId)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats publishing has started.
	 * 
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsPublishingStartedEvent()
	{
		return builder()
				.withPhase(ProcessingPhase.StatsPublishing)
				.withAction(Action.Started)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats publishing has started.
	 * 
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsPublishingStartedEvent(final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsPublishing)
				.withAction(Action.Started)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats publishing has started for a specific
	 * customer.
	 * 
	 * @param argCustomerId Customer ID.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsPublishingStartedEvent(final String argCustomerId)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsPublishing)
				.withAction(Action.Started)
				.withCustomerId(argCustomerId)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats publishing has started for a specific
	 * customer.
	 * 
	 * @param argCustomerId Customer ID.
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsPublishingStartedEvent(final String argCustomerId,
			final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsPublishing)
				.withAction(Action.Started)
				.withCustomerId(argCustomerId)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats publishing has finished for all customers.
	 * 
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsPublishingFinishedEvent()
	{
		return builder()
				.withPhase(ProcessingPhase.StatsPublishing)
				.withAction(Action.Finished)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats publishing has finished for all customers.
	 * 
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsPublishingFinishedEvent(final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsPublishing)
				.withAction(Action.Finished)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats publishing has finished for a specific
	 * customer.
	 * 
	 * @param argCustomerId Customer ID.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsPublishingFinishedEvent(final String argCustomerId)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsPublishing)
				.withAction(Action.Finished)
				.withCustomerId(argCustomerId)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that the stats publishing has finished for a specific
	 * customer.
	 * 
	 * @param argCustomerId Customer ID.
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsPublishingFinishedEvent(final String argCustomerId,
			final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsPublishing)
				.withAction(Action.Finished)
				.withCustomerId(argCustomerId)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that an error occurred during stats publishing.
	 * 
	 * @param argError Processing error.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsPublishingError(final ProcessingError argError)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsPublishing)
				.withAction(Action.ErrorOccurred)
				.withErrorDetails(argError)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that an error occurred during stats publishing.
	 * 
	 * @param argError Processing error.
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsPublishingError(final ProcessingError argError,
			final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsPublishing)
				.withAction(Action.ErrorOccurred)
				.withMetadata(argMetadata)
				.withErrorDetails(argError)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that an error occurred during stats publishing.
	 * 
	 * @param argError Processing error.
	 * @param argCustomerId Customer ID.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsPublishingError(final ProcessingError argError, final String argCustomerId)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsPublishing)
				.withAction(Action.ErrorOccurred)
				.withErrorDetails(argError)
				.withCustomerId(argCustomerId)
				.build();
	}

	/**
	 * Create a new instance of ProcessingEvent indicating that an error occurred during stats publishing.
	 * 
	 * @param argError Processing error.
	 * @param argCustomerId Customer ID.
	 * @param argMetadata Metadata.
	 * @return Processing event.
	 */
	public static ProcessingEvent newStatsPublishingError(final ProcessingError argError, final String argCustomerId,
			final Map<String, Object> argMetadata)
	{
		return builder()
				.withPhase(ProcessingPhase.StatsPublishing)
				.withAction(Action.ErrorOccurred)
				.withErrorDetails(argError)
				.withCustomerId(argCustomerId)
				.withMetadata(argMetadata)
				.build();
	}

	/**
	 * Create a new instance of SolarStatsProcessingEvent.
	 */
	protected ProcessingEvent()
	{
		this.id = UUID.randomUUID();
		this.timestamp = new Date();
	}

	/**
	 * Get event name.
	 * 
	 * @return Event name.
	 */
	@JsonProperty(value = "eventName", access = Access.READ_ONLY)
	@NotNull(message = "{validation.processingevent.eventname.required}")
	public String getEventName()
	{
		return EVENT;
	}

	/**
	 * Get version of the event.
	 *
	 * @return Event version.
	 */
	@JsonProperty(value = "version", required = true)
	@NotNull(message = "{validation.processingevent.version.required}")
	public String getVersion()
	{
		return this.version;
	}

	/**
	 * Set the version of the event.
	 *
	 * @param argVersion Event version.
	 */
	protected void setVersion(final String argVersion)
	{
		this.version = argVersion;
	}

	/**
	 * Get the ID of the event.
	 *
	 * @return Event ID.
	 */
	@JsonProperty(value = "id", required = true)
	@NotNull(message = "{validation.processingevent.id.required}")
	public UUID getId()
	{
		return this.id;
	}

	/**
	 * Set the ID of the event.
	 *
	 * @param argId Event ID.
	 */
	protected void setId(final UUID argId)
	{
		this.id = argId;
	}

	/**
	 * Get the correlation ID of the event.
	 *
	 * @return Correlation ID.
	 */
	@JsonProperty(value = "correlationId")
	public String getCorrelationId()
	{
		return this.correlationId;
	}

	/**
	 * Set the correlation ID of the event.
	 *
	 * @param argId Correlation ID.
	 */
	protected void setCorrelationId(final String argId)
	{
		this.correlationId = argId;
	}

	/**
	 * Get the processing phase.
	 *
	 * @return Processing phase.
	 */
	@JsonProperty(value = "phase", required = true)
	@NotNull(message = "{validation.processingevent.phase.required}")
	public ProcessingPhase getPhase()
	{
		return this.phase;
	}

	/**
	 * Set the processing phase.
	 *
	 * @param argPhase Processing phase.
	 */
	protected void setPhase(final ProcessingPhase argPhase)
	{
		this.phase = argPhase;
	}

	/**
	 * Get the action.
	 *
	 * @return Action.
	 */
	@JsonProperty(value = "action", required = true)
	@NotNull(message = "{validation.processingevent.action.required}")
	public Action getAction()
	{
		return this.action;
	}

	/**
	 * Set the action.
	 *
	 * @param argAction Action.
	 */
	protected void setAction(final Action argAction)
	{
		this.action = argAction;
	}

	/**
	 * Get the timestamp.
	 *
	 * @return Timestamp.
	 */
	@JsonProperty(value = "timestamp", required = true)
	@NotNull(message = "{validation.processingevent.timestamp.required}")
	public Date getTimestamp()
	{
		return this.timestamp;
	}

	/**
	 * Set the timestamp.
	 *
	 * @param argTimestamp Timestamp.
	 */
	protected void setTimestamp(final Date argTimestamp)
	{
		this.timestamp = argTimestamp;
	}

	/**
	 * Get the error details.
	 *
	 * @return Error details.
	 */
	@JsonProperty(value = "errorDetail")
	@Valid
	public ProcessingError getErrorDetails()
	{
		return this.errorDetails;
	}

	/**
	 * Set the error details.
	 *
	 * @param argError Error details.
	 */
	protected void setErrorDetails(final ProcessingError argError)
	{
		this.errorDetails = argError;
	}

	/**
	 * Get customer ID associated with the event.
	 *
	 * @return Customer ID.
	 */
	@JsonProperty(value = "customerId")
	public String getCustomerId()
	{
		return this.customerId;
	}

	/**
	 * Set the customer ID associated with the event.
	 *
	 * @param argCustomerId Customer ID.
	 */
	protected void setCustomerId(final String argCustomerId)
	{
		this.customerId = argCustomerId;
	}

	/**
	 * Get metadata associated with the event.
	 * 
	 * @return Metadata associated with the event.
	 */
	@JsonProperty(value = "metadata")
	public Map<String, Object> getMetadata()
	{
		return this.metadata;
	}

	/**
	 * Set metadata associated with the event.
	 * 
	 * @param argMetadata Event metadata.
	 */
	protected void setMetadata(final Map<String, Object> argMetadata)
	{
		this.metadata = argMetadata;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode()
	{
		return Objects.hash(EVENT, this.version, this.id, this.action, this.phase, this.customerId, this.errorDetails,
				this.metadata, this.timestamp);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final ProcessingEvent other = (ProcessingEvent) obj;
		return Objects.equals(this.version, other.version)
				&& Objects.equals(this.id, other.id)
				&& this.action == other.action
				&& this.phase == other.phase
				&& Objects.equals(this.customerId, other.customerId)
				&& Objects.equals(this.errorDetails, other.errorDetails)
				&& Objects.equals(this.metadata, other.metadata)
				&& Objects.equals(this.timestamp, other.timestamp);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString()
	{
		// @formatter:off
		final StringBuilder builder = new StringBuilder();
		builder.append("ProcessingEvent[")
				.append(getEventName())
				.append(":")
				.append(this.version)
				.append("; id=")
				.append(this.id)
				.append("; correlationId=")
				.append(this.correlationId != null ? this.correlationId : "null")
				.append("; phase=")
				.append(this.phase != null ? this.phase.getCode() : "null")
				.append("; action=")
				.append(this.action != null ? this.action.getValue() : "null")
				.append("; customerId=")
				.append(this.customerId)
				.append("; errorDetails=")
				.append(this.errorDetails != null ? this.errorDetails.toString() : "null")
				.append("; timestamp=")
				.append(this.timestamp)
				.append("; metadata=")
				.append(this.metadata != null
						? this.metadata.entrySet()
								.stream()
								.map(Map.Entry::toString)
								.collect(Collectors.joining("; ", "{", "}"))
								.toString()
						: "null")
				.append("]");
		return builder.toString();
		// @formatter:on
	}

	/**
	 * Action.
	 */
	public enum Action
	{
		/** A phase of the application execution has been requested. */
		Requested("requested"),

		/** A phase of the application execution has begun. */
		Started("start"),

		/** A phase of the application execution has finished. */
		Finished("finish"),

		/** An error has occurred. */
		ErrorOccurred("error");

		/** Internal value. */
		private String value;

		/**
		 * Create new type enumeration.
		 *
		 * @param argValue Internal value.
		 */
		private Action(final String argValue)
		{
			this.value = argValue;
		}

		/**
		 * Get the value of this event type.
		 *
		 * @return Value of this event type.
		 */
		@JsonValue
		public String getValue()
		{
			return this.value;
		}

		/**
		 * Get the value of this event type.
		 *
		 * @return Value of this event type.
		 */
		@Override
		public String toString()
		{
			return String.valueOf(this.value);
		}

		/**
		 * Get the enumeration that corresponds to the specified value.
		 *
		 * @param argValue Value used to look up an event type.
		 * @return Event type enumeration or {@code null} no enumeration has the specified value.
		 */
		@JsonCreator
		public static Action fromValue(final String argValue)
		{
			for (final Action b : Action.values())
			{
				if (String.valueOf(b.value).equals(argValue))
				{
					return b;
				}
			}
			return null;
		}

	}

	/**
	 * Instance builder.
	 *
	 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
	 */
	public static class Builder
	{
		/** Event being constructed. */
		private final ProcessingEvent event;

		/**
		 * Create a new instance of Builder.
		 */
		protected Builder()
		{
			this.event = new ProcessingEvent();
		}

		/**
		 * Set the ID for the event.
		 * 
		 * @param argId ID.
		 * @return Builder.
		 */
		public Builder withId(final UUID argId)
		{
			this.event.setId(argId);
			return this;
		}

		/**
		 * Set the correlation ID for the event.
		 * 
		 * @param argId Correlation ID.
		 * @return Builder.
		 */
		public Builder withCorrelationId(final String argId)
		{
			this.event.setCorrelationId(argId);
			return this;
		}

		/**
		 * Set the processing phase for the event.
		 * 
		 * @param argPhase Processing phase.
		 * @return Builder.
		 */
		public Builder withPhase(final ProcessingPhase argPhase)
		{
			this.event.setPhase(argPhase);

			return this;
		}

		/**
		 * Set the action of the event.
		 * 
		 * @param argAction Action.
		 * @return Builder.
		 */
		public Builder withAction(final Action argAction)
		{
			this.event.setAction(argAction);

			return this;
		}

		/**
		 * Set the timestamp of the event.
		 * 
		 * @param argTimestamp Timestamp.
		 * @return Builder.
		 */
		public Builder withTimestamp(final Date argTimestamp)
		{
			this.event.setTimestamp(argTimestamp);

			return this;
		}

		/**
		 * Set the version of the event.
		 *
		 * @param argVersion Event version.
		 * @return Processing event.
		 */
		public Builder withVersion(final String argVersion)
		{
			this.event.setVersion(argVersion);

			return this;
		}

		/**
		 * Set the error details of the event
		 *
		 * @param argError Error details.
		 * @return Processing event.
		 */
		@JsonProperty(value = "errorDetail")
		public Builder withErrorDetails(final ProcessingError argError)
		{
			this.event.setErrorDetails(argError);

			return this;
		}

		/**
		 * Set the verison of the event.
		 *
		 * @param argCustomerId Customer ID.
		 * @return Processing event.
		 */
		public Builder withCustomerId(final String argCustomerId)
		{
			this.event.setCustomerId(argCustomerId);

			return this;
		}

		/**
		 * Set metadata associated with the event.
		 * 
		 * @param argMetadata Event metadata.
		 * @return Processing event.
		 */
		public Builder withMetadata(final Map<String, Object> argMetadata)
		{
			this.event.setMetadata(argMetadata);

			return this;
		}

		/**
		 * Return a new processing event.
		 * 
		 * @return Processing event.
		 */
		public ProcessingEvent build()
		{
			return this.event;
		}

	}

}
