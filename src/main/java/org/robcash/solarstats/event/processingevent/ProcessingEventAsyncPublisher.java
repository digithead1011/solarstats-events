/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import org.robcash.solarstats.event.AbstractEventBridgePublisher;
import org.robcash.solarstats.event.AsyncEventPublisher;
import org.robcash.solarstats.event.PublishResult;
import org.robcash.solarstats.event.config.EventBridgeConfigProperties;
import org.robcash.utils.tracing.annotation.Traced;
import org.robcash.utils.tracing.aws.XRayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;

import reactor.core.publisher.Mono;
import software.amazon.awssdk.services.eventbridge.EventBridgeAsyncClient;
import software.amazon.awssdk.services.eventbridge.model.PutEventsRequestEntry;
import software.amazon.awssdk.services.eventbridge.model.PutEventsResponse;

/**
 * Publisher that sends processing events to EventBridge.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class ProcessingEventAsyncPublisher extends AbstractEventBridgePublisher
		implements AsyncEventPublisher<ProcessingEvent, PublishResult>
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(ProcessingEventAsyncPublisher.class);

	/** Event Bridge client. */
	protected final EventBridgeAsyncClient client;

	/**
	 * Create new instance of ProcessingEventAsyncPublisher.
	 */
	protected ProcessingEventAsyncPublisher()
	{
		super();
		this.client = null;
	}

	/**
	 * Create new instance of ProcessingEventAsyncPublisher.
	 *
	 * @param argClient EventBridge client.
	 * @param argConfig EventBridge configuration properties.
	 */
	public ProcessingEventAsyncPublisher(final EventBridgeAsyncClient argClient,
			final EventBridgeConfigProperties argConfig)
	{
		super(argConfig);
		Assert.notNull(argClient, "The EventBridge client cannot be null");
		this.client = argClient;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return When the event is successfully published, a single {@link PublishResult} object will be produced, which
	 *         will contain additional information. If the event publishing fails with a foreseeable error, a single
	 *         {@link PublishResult} object will be produced with details of the error. If an unforseeable error occurs,
	 *         however, e.g., an exception is thrown, then the publisher will return an error.
	 */
	@Override
	@Traced("Publish ProcessingEvent")
	public Mono<PublishResult> publishEvent(final ProcessingEvent argEvent, final Map<String, Object> argMetadata)
	{
		Assert.notNull(argEvent, "The event to publish cannot be null");

		final Optional<String> traceHeader = XRayUtils.getXRayTraceId();

		// Create merged copy of metadata
		final Map<String, Object> mergedMetadata = new HashMap<>();
		if (argEvent.getMetadata() != null)
		{
			mergedMetadata.putAll(argEvent.getMetadata());
		}
		if (argMetadata != null)
		{
			mergedMetadata.putAll(argMetadata);
		}

		// Publish an event to EventBridge
		@SuppressWarnings("unchecked")
		final CompletableFuture<PutEventsResponse> promise = this.client.putEvents(req -> {
			req.entries((Consumer<PutEventsRequestEntry.Builder>) entryBuilder -> {
				try
				{
					entryBuilder.source(this.config.getEventSource())
							.detailType(argEvent.getEventName())
							.detail(this.mapper.writeValueAsString(argEvent))
							.traceHeader(traceHeader.orElse(null));
					if (this.config.isCustomEventBus())
					{
						entryBuilder.eventBusName(this.config.getCustomEventBusName());
					}
				}
				catch (final JsonProcessingException ex)
				{
					LOG.warn("Failed to send processing event due to serialization error", ex);
				}
			});
		});

		// Handle response
		return Mono.fromFuture(promise)

				// Log stream processing with default category at FINE (debug) level
				.log(null, java.util.logging.Level.FINE)

				// Get PutEventsResultEntry entries
				.flatMapIterable(response -> response.entries())

				// But only the first one since EventBridge returns a response entry for every request entry
				.elementAt(0)

				.map(result -> {
					// Create result builder
					PublishResult.Builder resultBuilder = PublishResult.builder();

					// Inspect event ID to see if event was published successfully
					final String eventId = result.eventId();
					if (eventId == null)
					{
						// Event was not published to EventBridge successfully
						logPublishFailure(argEvent.getEventName(), result.errorCode(), result.errorMessage());
						resultBuilder = resultBuilder.withSuccess(false)
								.withErrorCode(result.errorCode())
								.withErrorMessage(result.errorMessage());
					}
					else
					{
						// Event was published to EventBridge successfully
						resultBuilder = resultBuilder.withSuccess(true)
								.withEventId(eventId);

						// No error - whew!
						LOG.debug("Successfully published processing event with detail type of {} to EventBridge: "
								+ "eventId is {}", argEvent.getEventName(), result.eventId());
					}

					return resultBuilder.build();
				})

				// Handle errors
				.doOnError(t -> {
					logPublishFailure(argEvent.getEventName(), t);
				});
	}
}
