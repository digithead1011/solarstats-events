/*
 * Copyright (C) 2024 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Processing phase.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public enum ProcessingPhase
{

	/** Event is related to inquiry of customers. */
	CustomerRetrieval("CustomerRetrieval", Integer.valueOf(1)),

	/** Event is related to inquiry of production or usage data. */
	StatsRetrieval("StatsRetrieval", Integer.valueOf(2)),

	/** Event is related to publishing of production or usage data. */
	StatsPublishing("StatsPublishing", Integer.valueOf(4));

	private String code;

	private Integer value;

	/**
	 * Create new processing phase enumeration.
	 *
	 * @param argCode Internal code.
	 * @param argValue Bitwise value.
	 */
	private ProcessingPhase(final String argCode, final Integer argValue)
	{
		this.code = argCode;
		this.value = argValue;
	}

	/**
	 * Get the code of this step.
	 *
	 * @return Code of this step.
	 */
	@JsonValue
	public String getCode()
	{
		return this.code;
	}

	/**
	 * Get the bitwise value of this phase.
	 *
	 * @return Bitwise value of this phase.
	 */
	@JsonIgnore
	public Integer getValue()
	{
		return this.value;
	}

	/**
	 * Get the code of this step.
	 *
	 * @return Value of this step.
	 */
	@Override
	public String toString()
	{
		return String.valueOf(this.code);
	}

	/**
	 * Get the enumeration that corresponds to the specified code.
	 *
	 * @param argCode Code used to look up a step.
	 * @return Processing phase enumeration or {@code null} no enumeration has the specified code.
	 */
	@JsonCreator
	public static ProcessingPhase fromCode(final String argCode)
	{
		for (final ProcessingPhase p : ProcessingPhase.values())
		{
			if (String.valueOf(p.code).equals(argCode))
			{
				return p;
			}
		}
		return null;
	}

}
