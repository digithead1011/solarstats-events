/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import software.amazon.awssdk.services.sns.model.MessageAttributeValue;
import software.amazon.awssdk.services.sns.model.PublishRequest;

/**
 * Abstract SNS event publisher.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 * @param <E> Event type.
 */
public abstract class AbstractSNSEventPublisher<E>
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(AbstractSNSEventPublisher.class);

	/** SNS String data type. */
	protected static final String STRING_DATA_TYPE = "String";

	/** SNS topic name. */
	protected final String topicName;

	/** SNS topic ARN. */
	protected final String topicArn;

	/** Object Mapper. */
	@Autowired
	protected ObjectMapper mapper;

	/**
	 * Create a new event publisher that sends events to an SNS topic.
	 */
	protected AbstractSNSEventPublisher()
	{
		super();
		this.topicName = null;
		this.topicArn = null;
	}

	/**
	 * Create a new event publisher that sends events to an SNS topic.
	 *
	 * @param argTopicName SNS topic name.
	 * @param argTopicArn SNS topic ARN.
	 */
	public AbstractSNSEventPublisher(final String argTopicName, final String argTopicArn)
	{
		Assert.notNull(argTopicName, "The SNS topic name cannot be null");
		Assert.notNull(argTopicArn, "The SNS topic ARN cannot be null");

		this.topicName = argTopicName;
		this.topicArn = argTopicArn;
	}

	/**
	 * Publish a message to SNS.
	 * 
	 * @param argEvent Event to publish.
	 * @param argCustomerId Customer ID.
	 * @param argSubject Message subject.
	 * @param argMetadata Metadata.
	 * @return Result from publishing.
	 * @throws JsonProcessingException Thrown when event cannot be serialized into a message.
	 */
	protected PublishRequest createRequest(final E argEvent, final String argCustomerId, final String argSubject,
			final Map<String, Object> argMetadata) throws JsonProcessingException
	{
		Assert.notNull(argEvent, "Event to publish cannot be null");
		Assert.notNull(argSubject, "Message subject cannot be null");
		Assert.notNull(argMetadata, "Metadata cannot be null");

		// Create message
		final String message = this.mapper.writeValueAsString(argEvent);
		final Map<String, MessageAttributeValue> attributes = new HashMap<>();

		// So why put the subject as metadata?
		// Well, SNS filtering can only be done on messages attributes and there has to be at least one
		// attribute in order for it to work correctly. If the message has no attributes and there's a
		// filter criteria that matches the lack of an attribute, then surprisingly that doesn't match at all.
		// In order to match the lack of an attribute, there has to be one other attribute present. This
		// is intended to be that attribute.
		attributes.put("event", MessageAttributeValue.builder()
				.dataType(STRING_DATA_TYPE)
				.stringValue(argSubject)
				.build());

		// Add any other attributes provided in the metadata
		argMetadata.entrySet()
				.stream()
				.filter(entry -> String.class.isInstance(entry.getValue()))
				.forEach(entry -> {
					attributes.put(entry.getKey(), MessageAttributeValue.builder()
							.dataType(STRING_DATA_TYPE)
							.stringValue((String) entry.getValue())
							.build());
				});

		// Create request
		final PublishRequest request = PublishRequest.builder()
				.topicArn(this.topicArn)
				.subject(argSubject)
				.message(message)
				.messageAttributes(attributes)
				.build();

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Message to publish: {}", request.message());
			LOG.debug("Message attributes: {}", request.messageAttributes().entrySet()
					.stream()
					.map(entry -> {
						return entry.getKey() + " = " + entry.getValue().toString();
					})
					.collect(Collectors.joining("; ")));
		}

		return request;
	}
}
