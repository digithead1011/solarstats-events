/*
 * Copyright (C) 2022-2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event;

import java.util.Map;

import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;

import jakarta.validation.Valid;

/**
 * Event publisher.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 * @param <E> Type of event that is published.
 * @param <R> Type of response that is returned.
 */
@Validated
public interface EventPublisher<E, R>
{

	/**
	 * Publish an event.
	 *
	 * @param argEvent Event.
	 * @return Publishing result.
	 */
	default R publishEvent(@Valid final E argEvent)
	{
		Assert.notNull(argEvent, "The event to publish cannot be null");

		return this.publishEvent(argEvent, null);
	}

	/**
	 * Publish an event with metadata.
	 *
	 * @param argEvent Event.
	 * @param argMetadata Metadata to be included in the event that is published.
	 * @return Publishing result.
	 */
	R publishEvent(@Valid E argEvent, Map<String, Object> argMetadata);

}
