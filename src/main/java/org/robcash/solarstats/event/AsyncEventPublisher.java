/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event;

import reactor.core.publisher.Mono;

/**
 * Asynchronous event publisher.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 * @param <E> Type of event that is published.
 * @param <R> Type of response that is returned.
 */
public interface AsyncEventPublisher<E, R> extends EventPublisher<E, Mono<R>>
{

}
