/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.energymetrics;

import java.util.Map;
import java.util.UUID;

import org.robcash.solarstats.event.PublishResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * No-op event processor that does swallows EnergyMetrics events without doing anything.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class NoopEnergyMetricsPublisher extends EnergyMetricsPublisher
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(NoopEnergyMetricsPublisher.class);

	/**
	 * Create new instance of NoopEventPublisher.
	 */
	public NoopEnergyMetricsPublisher()
	{
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PublishResult publishEvent(final EnergyMetricsEvent argEvent, final Map<String, Object> argMetadata)
	{
		Assert.notNull(argEvent, "The event to publish cannot be null");
		Assert.notNull(argEvent.getMetrics(), "The event to publish cannot have null metrics");

		LOG.info("No-op event publisher will skip publishing EnergyMetrics event");

		if (LOG.isDebugEnabled())
		{
			try
			{
				LOG.debug(this.mapper.writeValueAsString(argEvent));
			}
			catch (final JsonProcessingException ex)
			{
				LOG.warn("Failed to log EnergyMetricsEvent as a String", ex);
			}
		}

		return PublishResult.builder()
				.withSuccess(true)
				.withEventId("noop-" + UUID.randomUUID().toString())
				.build();
	}

}
