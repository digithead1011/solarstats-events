/*
 * Copyright (C) 2022-2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.energymetrics.config;

import org.robcash.solarstats.event.config.EventsConfigProperties;
import org.robcash.solarstats.event.energymetrics.EnergyMetricsAsyncPublisher;
import org.robcash.solarstats.event.energymetrics.EnergyMetricsPublisher;
import org.robcash.solarstats.event.energymetrics.NoopEnergyMetricsAsyncPublisher;
import org.robcash.solarstats.event.energymetrics.NoopEnergyMetricsPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import software.amazon.awssdk.services.eventbridge.EventBridgeAsyncClient;
import software.amazon.awssdk.services.eventbridge.EventBridgeClient;

/**
 * Spring beans for publishing EnergyMetricsEvent events.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(EventsConfigProperties.class)
@ConditionalOnProperty(prefix = "solarstats.events.energy-metrics", name = "event-source")
public class EnergyMetricsConfig
{
	@Autowired
	private EventsConfigProperties config;

	@Bean
	@ConditionalOnMissingBean
	EventBridgeClient eventBridgeClient()
	{
		return EventBridgeClient.create();
	}

	@Bean
	@ConditionalOnMissingBean
	EventBridgeAsyncClient eventBridgeAsyncClient()
	{
		return EventBridgeAsyncClient.create();
	}

	@Bean
	@ConditionalOnMissingBean
	EnergyMetricsPublisher energyMetricsPublisher(final EventBridgeClient argClient)
	{
		if (this.config.isEnabled())
		{
			return new EnergyMetricsPublisher(argClient, this.config.getEnergyMetrics());
		}
		else
		{
			return new NoopEnergyMetricsPublisher();
		}
	}

	@Bean
	@ConditionalOnMissingBean
	EnergyMetricsAsyncPublisher asyncEnergyMetricsPublisher(final EventBridgeAsyncClient argClient)
	{
		if (this.config.isEnabled())
		{
			return new EnergyMetricsAsyncPublisher(argClient, this.config.getEnergyMetrics());
		}
		else
		{
			return new NoopEnergyMetricsAsyncPublisher();
		}
	}

}
