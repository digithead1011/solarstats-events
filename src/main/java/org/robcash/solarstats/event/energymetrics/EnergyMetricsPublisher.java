/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.energymetrics;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import org.robcash.solarstats.event.AbstractEventBridgePublisher;
import org.robcash.solarstats.event.EventPublisher;
import org.robcash.solarstats.event.PublishResult;
import org.robcash.solarstats.event.config.EventBridgeConfigProperties;
import org.robcash.utils.tracing.annotation.Traced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;

import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.services.eventbridge.EventBridgeClient;
import software.amazon.awssdk.services.eventbridge.model.PutEventsRequest;
import software.amazon.awssdk.services.eventbridge.model.PutEventsRequestEntry;
import software.amazon.awssdk.services.eventbridge.model.PutEventsResponse;
import software.amazon.awssdk.services.eventbridge.model.PutEventsResultEntry;

/**
 * Event publisher that publishes energy metrics to EventBridge.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class EnergyMetricsPublisher extends AbstractEventBridgePublisher
		implements EventPublisher<EnergyMetricsEvent, PublishResult>
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(EnergyMetricsPublisher.class);

	/** Event Bridge client. */
	protected final EventBridgeClient client;

	/**
	 * Create new instance of EnergyMetricsPublisher.
	 */
	protected EnergyMetricsPublisher()
	{
		super();
		this.client = null;
	}

	/**
	 * Create new instance of EnergyMetricsPublisher.
	 *
	 * @param argClient EventBridge client.
	 * @param argConfig EventBridge configuration properties.
	 */
	public EnergyMetricsPublisher(final EventBridgeClient argClient, final EventBridgeConfigProperties argConfig)
	{
		super(argConfig);

		Assert.notNull(argClient, "The EventBridge client cannot be null");
		this.client = argClient;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return {@link PublishResult} is returned, which contains information about the success or failure of the
	 *         publishing.
	 */
	@Override
	@Traced("Publish EnergyMetrics")
	public PublishResult publishEvent(final EnergyMetricsEvent argEvent, final Map<String, Object> argMetadata)
	{
		Assert.notNull(argEvent, "The event to publish cannot be null");
		Assert.notNull(argEvent.getMetrics(), "The event to publish cannot have null metrics");

		PublishResult.Builder resultBuilder = PublishResult.builder();

		// Create merged copy of metadata
		final Map<String, Object> mergedMetadata = new HashMap<>();
		if (argEvent.getMetadata() != null)
		{
			mergedMetadata.putAll(argEvent.getMetadata());
		}
		if (argMetadata != null)
		{
			mergedMetadata.putAll(argMetadata);
		}

		// Build request
		@SuppressWarnings("unchecked")
		final PutEventsRequest request = PutEventsRequest.builder()
				.entries((Consumer<PutEventsRequestEntry.Builder>) entryBuilder -> {
					try
					{
						entryBuilder.source(this.config.getEventSource())
								.detailType(argEvent.getEventName())
								.detail(this.mapper.writeValueAsString(argEvent));
						if (this.config.isCustomEventBus())
						{
							entryBuilder.eventBusName(this.config.getCustomEventBusName());
						}
					}
					catch (final JsonProcessingException ex)
					{
						LOG.warn("Failed to send energy metrics event due to serialization error", ex);
					}
				})
				.build();

		try
		{
			// Publish message
			final PutEventsResponse response = this.client.putEvents(request);

			// Handle response
			final List<PutEventsResultEntry> entries = Optional.ofNullable(response.entries())
					.or(() -> {
						return Optional.of(Collections.emptyList());
					})
					.get();
			for (final PutEventsResultEntry result : entries)
			{
				final String eventId = result.eventId();
				if (eventId != null)
				{
					// No error - whew!
					resultBuilder = resultBuilder.withSuccess(true)
							.withEventId(eventId);

					if (LOG.isDebugEnabled())
					{
						LOG.debug("Successfully published energy metrics event with detail type of {} to "
								+ "EventBridge: eventId is {}", argEvent.getEventName(), result.eventId());
					}
				}
				else
				{
					// Entry did not contain an Event ID, therefore the event was not published successfully
					logPublishFailure(argEvent.getEventName(), result.errorCode(), result.errorMessage());

					resultBuilder = resultBuilder.withSuccess(false)
							.withErrorCode(result.errorCode())
							.withErrorMessage(result.errorMessage());
				}

				// Because we only put a single event, there should only be a single event in the response
				break;
			}
		}
		catch (final SdkException ex)
		{
			logPublishFailure(argEvent.getEventName(), ex);
			resultBuilder = resultBuilder.withSuccess(false)
					.withException(ex)
					.withErrorMessage(ex.getMessage());
		}

		return resultBuilder.build();
	}

}
