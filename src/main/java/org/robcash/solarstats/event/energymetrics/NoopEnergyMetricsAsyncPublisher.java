/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.energymetrics;

import java.util.Map;
import java.util.UUID;

import org.robcash.solarstats.event.PublishResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import reactor.core.publisher.Mono;

/**
 * No-op async publisher that swallows energy metrics events without doing anything.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class NoopEnergyMetricsAsyncPublisher extends EnergyMetricsAsyncPublisher
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(NoopEnergyMetricsAsyncPublisher.class);

	/**
	 * Create new instance of NoopEnergyMetricsAsyncPublisher.
	 */
	public NoopEnergyMetricsAsyncPublisher()
	{
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Mono<PublishResult> publishEvent(final EnergyMetricsEvent argEvent, final Map<String, Object> argMetadata)
	{
		Assert.notNull(argEvent, "The event to publish cannot be null");
		Assert.notNull(argEvent.getMetrics(), "The event to publish cannot have null metrics");

		LOG.info("No-op event publisher will skip publishing EnergyMetrics event");

		final PublishResult result = PublishResult.builder()
				.withEventId("noop-" + UUID.randomUUID().toString())
				.build();
		return Mono.just(result);
	}
}
