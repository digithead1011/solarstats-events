/*
 * Copyright (C) 2022-2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.energymetrics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.robcash.solarstats.monitoring.model.EnergyMetrics;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

/**
 * Energy Metrics Event.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@JsonPropertyOrder({ "eventName", "version", "id", "correlationId", "action", "customerId", "metrics", "metadata" })
@JsonDeserialize(builder = EnergyMetricsEvent.Builder.class)
public class EnergyMetricsEvent implements Serializable
{
	/** Serialization ID. */
	private static final long serialVersionUID = 1L;

	/** Event. */
	private static final String EVENT = "SolarStatsMetricsEvent";

	/** Version. */
	public static final String VERSION = "1";

	/** Version. */
	private String version = VERSION;

	/** ID. */
	private UUID id;

	/** Correlation ID. */
	private String correlationId;

	/** Action. */
	private Action action;

	/** Customer ID. */
	private String customerId;

	/** Energy metrics. */
	private final List<EnergyMetrics> metrics;

	/** Metadata. */
	private Map<String, Object> metadata;

	/**
	 * Create a new builder that can construct a new EnergyMetricsEvent.
	 * 
	 * @return EnergyMetricsEvent builder.
	 */
	public static Builder builder()
	{
		return new Builder();
	}

	/**
	 * Create a new instance of EnergyMetricsEvent.
	 */
	protected EnergyMetricsEvent()
	{
		this.id = UUID.randomUUID();
		this.metrics = new ArrayList<>();
	}

	/**
	 * Get event name.
	 * 
	 * @return Event name.
	 */
	@JsonProperty(value = "eventName", access = Access.READ_ONLY)
	@NotNull(message = "{validation.energymetricsevent.eventname.required}")
	public String getEventName()
	{
		return EVENT;
	}

	/**
	 * Get version of the event.
	 *
	 * @return Event version.
	 */
	@JsonProperty(value = "version", required = true)
	@NotNull(message = "{validation.energymetricsevent.version.required}")
	public String getVersion()
	{
		return this.version;
	}

	/**
	 * Set the version of the event.
	 *
	 * @param argVersion Event version.
	 */
	public void setVersion(final String argVersion)
	{
		this.version = argVersion;
	}

	/**
	 * Get the ID of the event.
	 *
	 * @return Event ID.
	 */
	@JsonProperty(value = "id", required = true)
	@NotNull(message = "{validation.energymetricsevent.id.required}")
	public UUID getId()
	{
		return this.id;
	}

	/**
	 * Set the ID of the event.
	 *
	 * @param argId Event ID.
	 */
	protected void setId(final UUID argId)
	{
		this.id = argId;
	}

	/**
	 * Get the correlation ID of the event.
	 *
	 * @return Correlation ID.
	 */
	@JsonProperty(value = "correlationId")
	public String getCorrelationId()
	{
		return this.correlationId;
	}

	/**
	 * Set the correlation ID of the event.
	 *
	 * @param argId Correlation ID.
	 */
	protected void setCorrelationId(final String argId)
	{
		this.correlationId = argId;
	}

	/**
	 * Get the action.
	 *
	 * @return Action.
	 */
	@JsonProperty(value = "action", required = true)
	@NotNull(message = "{validation.energymetricsevent.action.required}")
	public Action getAction()
	{
		return this.action;
	}

	/**
	 * Set the action.
	 *
	 * @param argAction Event action.
	 */
	protected void setAction(final Action argAction)
	{
		this.action = argAction;
	}

	/**
	 * Get customer ID associated with the event.
	 *
	 * @return Customer ID.
	 */
	@JsonProperty(value = "customerId", required = true)
	@NotNull(message = "{validation.energymetricsevent.customerId.required}")
	public String getCustomerId()
	{
		return this.customerId;
	}

	/**
	 * Set the customer ID associated with the event.
	 *
	 * @param argCustomerId Customer ID.
	 */
	protected void setCustomerId(final String argCustomerId)
	{
		this.customerId = argCustomerId;
	}

	/**
	 * Get energy metrics.
	 *
	 * @return Energy metrics.
	 */
	@JsonProperty(value = "metrics", required = true)
	@Valid
	@NotEmpty(message = "{validation.energymetricsevent.metrics.required}")
	public List<EnergyMetrics> getMetrics()
	{
		return this.metrics;
	}

	/**
	 * Set the energy metrics.
	 *
	 * @param argMetrics Energy metrics.
	 */
	protected void setMetrics(final List<EnergyMetrics> argMetrics)
	{
		this.metrics.clear();
		if (argMetrics != null)
		{
			this.metrics.addAll(argMetrics);
		}
	}

	/**
	 * Add energy metrics.
	 *
	 * @param argMetrics Energy metrics.
	 */
	protected void addMetrics(final EnergyMetrics argMetrics)
	{
		this.metrics.add(argMetrics);
	}

	/**
	 * Get metadata associated with the event.
	 * 
	 * @return Metadata associated with the event.
	 */
	@JsonProperty(value = "metadata")
	public Map<String, Object> getMetadata()
	{
		return this.metadata;
	}

	/**
	 * Set metadata associated with the event.
	 * 
	 * @param argMetadata Event metadata.
	 */
	protected void setMetadata(final Map<String, Object> argMetadata)
	{
		this.metadata = argMetadata;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append(this.getClass().getSimpleName());
		builder.append(" [");
		builder.append("eventName=");
		builder.append(getEventName());
		builder.append("; version=");
		builder.append(this.version != null ? this.version : "(null)");
		builder.append("; action=");
		builder.append(this.action != null ? this.action.toString() : "(null)");
		builder.append("; metrics=");
		builder.append(this.metrics != null ? this.metrics.toString() : "(null)");
		builder.append("]");

		return builder.toString();
	}

	/**
	 * Action.
	 */
	public enum Action
	{
		/** Type that indicates the energy metrics have been retrieved. */
		MetricsRetrieved("SolarStatsRetrieved"),

		/** Type that indicates the energy metrics have been published. */
		MetricsPublished("SolarStatsPublished");

		/** Internal value. */
		private String value;

		/**
		 * Create new type enumeration.
		 *
		 * @param argValue Internal value.
		 */
		private Action(final String argValue)
		{
			this.value = argValue;
		}

		/**
		 * Get the value of this action.
		 *
		 * @return Value of this action.
		 */
		@JsonValue
		public String getValue()
		{
			return this.value;
		}

		/**
		 * Get the value of this event type.
		 *
		 * @return Value of this event type.
		 */
		@Override
		public String toString()
		{
			return String.valueOf(this.value);
		}

		/**
		 * Get the enumeration that corresponds to the specified value.
		 *
		 * @param argValue Value used to look up an event type.
		 * @return Event type enumeration or {@code null} no enumeration has the specified value.
		 */
		@JsonCreator
		public static Action fromValue(final String argValue)
		{
			for (final Action b : Action.values())
			{
				if (String.valueOf(b.value).equals(argValue))
				{
					return b;
				}
			}
			return null;
		}

	}

	/**
	 * Instance builder.
	 *
	 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
	 */
	public static class Builder
	{
		/** Event being constructed. */
		private final EnergyMetricsEvent event;

		/**
		 * Create a new instance of Builder.
		 */
		protected Builder()
		{
			this.event = new EnergyMetricsEvent();
		}

		/**
		 * Set the version of the event.
		 *
		 * @param argVersion Event version.
		 * @return Builder.
		 */
		public Builder withVersion(final String argVersion)
		{
			this.event.setVersion(argVersion);

			return this;
		}

		/**
		 * Set the ID for the event.
		 * 
		 * @param argId ID.
		 * @return Builder.
		 */
		public Builder withId(final UUID argId)
		{
			this.event.setId(argId);

			return this;
		}

		/**
		 * Set the correlation ID for the event.
		 * 
		 * @param argId Correlation ID.
		 * @return Builder.
		 */
		public Builder withCorrelationId(final String argId)
		{
			this.event.setCorrelationId(argId);

			return this;
		}

		/**
		 * Set the action of the event.
		 * 
		 * @param argAction Action.
		 * @return Builder.
		 */
		public Builder withAction(final Action argAction)
		{
			this.event.setAction(argAction);

			return this;
		}

		/**
		 * Set the verison of the event.
		 *
		 * @param argCustomerId Customer ID.
		 * @return Builder.
		 */
		public Builder withCustomerId(final String argCustomerId)
		{
			this.event.setCustomerId(argCustomerId);

			return this;
		}

		/**
		 * Add metrics to the event.
		 * 
		 * @param argMetrics Metrics.
		 * @return Builder.
		 */
		@JsonIgnore
		public Builder withMetrics(final EnergyMetrics argMetrics)
		{
			this.event.addMetrics(argMetrics);

			return this;
		}

		/**
		 * Add metrics to the event.
		 * 
		 * @param argMetrics Metrics.
		 * @return Builder.
		 */
		@JsonProperty("metrics")
		public Builder withMetrics(final List<EnergyMetrics> argMetrics)
		{
			this.event.setMetrics(argMetrics);

			return this;
		}

		/**
		 * Set metadata associated with the event.
		 * 
		 * @param argMetadata Event metadata.
		 * @return Builder.
		 */
		public Builder withMetadata(final Map<String, Object> argMetadata)
		{
			this.event.setMetadata(argMetadata);

			return this;
		}

		/**
		 * Return a new energy metrics event.
		 * 
		 * @return Energy metrics event.
		 */
		public EnergyMetricsEvent build()
		{
			return this.event;
		}
	}
}
