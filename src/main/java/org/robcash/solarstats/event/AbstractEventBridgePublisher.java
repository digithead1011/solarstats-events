/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event;

import org.robcash.solarstats.event.config.EventBridgeConfigProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Abstract Event Bridge publisher.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public abstract class AbstractEventBridgePublisher
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(AbstractEventBridgePublisher.class);

	/** Event Bridge config. */
	protected final EventBridgeConfigProperties config;

	/** Object Mapper. */
	@Autowired
	protected ObjectMapper mapper;

	/**
	 * Create new instance of AbstractEventBridgePublisher.
	 */
	protected AbstractEventBridgePublisher()
	{
		this.config = null;
	}

	/**
	 * Create new instance of AbstractEventBridgePublisher.
	 * 
	 * @param argConfig EventBridge config properties.
	 */
	public AbstractEventBridgePublisher(final EventBridgeConfigProperties argConfig)
	{
		Assert.notNull(argConfig, "The EventBridge config cannot be null");

		this.config = argConfig;
	}

	/**
	 * Log a publishing failure that did not result in an exception being thrown.
	 * 
	 * @param argEventDetailType Event detail type.
	 * @param argErrorCode Error code.
	 * @param argErrorMessage Error message.
	 */
	protected void logPublishFailure(final String argEventDetailType,
			final String argErrorCode, final String argErrorMessage)
	{
		LOG.error("Failed to publish event with detail type of {} to EventBridge: {} - {}",
				argEventDetailType, argErrorCode, argErrorMessage);
	}

	/**
	 * Log a publishing exception that resulted in an exception.
	 * 
	 * @param argEventDetailType Event detail type.
	 * @param argException Exception that was thrown.
	 */
	protected void logPublishFailure(final String argEventDetailType, final Throwable argException)
	{
		LOG.error("Failed to publish event with detail type of {} to EventBridge",
				argEventDetailType, argException);
	}

}
