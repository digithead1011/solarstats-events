/*
 * Copyright (C) 2022-2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.config;

import org.robcash.solarstats.event.energymetrics.config.EnergyMetricsConfig;
import org.robcash.solarstats.event.processingevent.config.ProcessingEventsConfig;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * Spring configuration for SolarStats Events.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@AutoConfiguration
@Import({ EnergyMetricsConfig.class, ProcessingEventsConfig.class })
public class EventsAutoConfiguration
{

	@Bean
	MessageSource eventsValidationMessageSource()
	{
		final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:/org/robcash/solarstats/event/validation");
		return messageSource;
	}

}
