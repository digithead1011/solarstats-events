/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.config;

import java.net.URI;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configuration properties for SolarStats events.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@ConfigurationProperties("solarstats.events")
public class EventsConfigProperties
{
	/**
	 * Create new instance of EventsConfigProperties.
	 */
	public EventsConfigProperties()
	{
		super();
	}

	/** Enabled flag. */
	private boolean enabled = true;

	/** SNS endpoint. */
	private URI snsEndpoint;

	/** EventBridge endpoint. */
	private URI eventBridgeEndpoint;

	/** EnergyMetrics publishing config. */
	private EventBridgeConfigProperties energyMetrics;

	/** ProcessingEvent publishing config. */
	private EventBridgeConfigProperties processingEvents;

	/** Error handling. */
	private SNSConfigProperties errors;

	/**
	 * Check the enabled flag.
	 * 
	 * @return {@code true} if the events capability is enabled.
	 */
	public boolean isEnabled()
	{
		return this.enabled;
	}

	/**
	 * Set the enabled flag.
	 * 
	 * @param argEnabled Enables or disables the event publishing.
	 */
	public void setEnabled(final boolean argEnabled)
	{
		this.enabled = argEnabled;
	}

	/**
	 * Get the SNS endpoint.
	 *
	 * @return SNS endpoint.
	 */
	public URI getSnsEndpoint()
	{
		return this.snsEndpoint;
	}

	/**
	 * Set the SNS endpoint.
	 *
	 * @param argEndpoint SNS endpoint.
	 */
	public void setSnsEndpoint(final URI argEndpoint)
	{
		this.snsEndpoint = argEndpoint;
	}

	/**
	 * Get the EventBridge endpoint.
	 *
	 * @return EventBridge endpoint.
	 */
	public URI getEventBridgeEndpoint()
	{
		return this.eventBridgeEndpoint;
	}

	/**
	 * Set the EventBridge endpoint.
	 *
	 * @param argEndpoint EventBridge endpoint.
	 */
	public void setEventBridgeEndpoint(final URI argEndpoint)
	{
		this.eventBridgeEndpoint = argEndpoint;
	}

	/**
	 * Get the energy metrics event publisher configuration properties.
	 *
	 * @return Energy metrics event publisher configuration properties.
	 */
	public EventBridgeConfigProperties getEnergyMetrics()
	{
		return this.energyMetrics;
	}

	/**
	 * Set the energy metrics event publisher configuration properties.
	 *
	 * @param argConfig Energy metrics event publisher configuration properties.
	 */
	public void setEnergyMetrics(final EventBridgeConfigProperties argConfig)
	{
		this.energyMetrics = argConfig;
	}

	/**
	 * Get the EventBridge configuration properties.
	 *
	 * @return EventBridge configuration properties.
	 */
	public EventBridgeConfigProperties getProcessingEvents()
	{
		return this.processingEvents;
	}

	/**
	 * Set the EventBridge configuration properties.
	 *
	 * @param argConfig EventBridge configuration properties.
	 */
	public void setProcessingEvents(final EventBridgeConfigProperties argConfig)
	{
		this.processingEvents = argConfig;
	}

	/**
	 * Get the error properties.
	 * 
	 * @return Error properties.
	 */
	public SNSConfigProperties getErrors()
	{
		return this.errors;
	}

	/**
	 * Set the error properties.
	 * 
	 * @param argErrors Error properties.
	 */
	public void setErrors(final SNSConfigProperties argErrors)
	{
		this.errors = argErrors;
	}

}
