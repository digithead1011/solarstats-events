/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.config;

/**
 * Configuration properties for communicating with AWS EventBridge.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class EventBridgeConfigProperties
{
	/** Event source. */
	private String eventSource;

	/** Custom bus name. */
	private String customEventBusName;

	/**
	 * Create new instance of EventBridgeConfigProperties.
	 */
	public EventBridgeConfigProperties()
	{
		super();
	}

	/**
	 * Get the default event source.
	 * 
	 * @return Default event source.
	 */
	public String getEventSource()
	{
		return this.eventSource;
	}

	/**
	 * Set the default event source.
	 * 
	 * @param argEventSource Event source.
	 */
	public void setEventSource(final String argEventSource)
	{
		this.eventSource = argEventSource;
	}

	/**
	 * Get the name of the custom event bus to which events should be published.
	 * 
	 * @return Custom event bus name.
	 */
	public String getCustomEventBusName()
	{
		return this.customEventBusName;
	}

	/**
	 * Set the name of the custom event bus to which events should be published. If this is set to {@code null} then the
	 * default event bus will be used.
	 * 
	 * @param argName Custom event bus name.
	 */
	public void setCustomEventBusName(final String argName)
	{
		this.customEventBusName = argName;
	}

	/**
	 * Indicates if the default event bus will be used to publish events.
	 * 
	 * @return Returns {@code true} when no custom event bus name has been set.
	 */
	public boolean isDefaultEventBus()
	{
		return this.customEventBusName == null;
	}

	/**
	 * Indicates if a custom event bus will be used to publish events.
	 * 
	 * @return Returns {@code true} when the custom event bus name has been set.
	 */
	public boolean isCustomEventBus()
	{
		return this.customEventBusName != null;
	}
}
