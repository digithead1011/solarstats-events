/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent.config;

import org.mockito.Mockito;
import org.robcash.solarstats.event.config.EventsConfigProperties;
import org.robcash.solarstats.event.processingevent.ProcessingEvent;
import org.robcash.solarstats.event.processingevent.ProcessingEventPublisher;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import software.amazon.awssdk.services.eventbridge.EventBridgeClient;

/**
 * Spring config for {@link ProcessingEventPublisherTest}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(EventsConfigProperties.class)
@Profile("test")
public class ProcessingEventPublisherTestConfig
{
	@Bean
	EventBridgeClient mockEBClient()
	{
		return Mockito.mock(EventBridgeClient.class);
	}

	@Bean
	EventBridgeClient actualEBClient()
	{
		return EventBridgeClient.builder()
				// .credentialsProvider(ProfileCredentialsProvider.create("uploader"))
				.build();
	}

	@Bean
	ProcessingEventPublisher mockPublisher(@Qualifier("mockEBClient") final EventBridgeClient argClient,
			final EventsConfigProperties argConfig)
	{
		return new ProcessingEventPublisher(argClient, argConfig.getProcessingEvents());
	}

	@Bean
	ProcessingEvent statsRetrievalStartedEvent()
	{
		return ProcessingEvent.newStatsRetrievalStartedEvent();
	}
}
