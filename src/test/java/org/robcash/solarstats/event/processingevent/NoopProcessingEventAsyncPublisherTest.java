/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent;

import java.time.Duration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.robcash.solarstats.event.processingevent.config.NoopProcessingEventPublisherTestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import reactor.test.StepVerifier;

/**
 * Unit tests for {@link NoopProcessingEventAsyncPublisher}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootTest(classes = NoopProcessingEventPublisherTestConfig.class)
@ActiveProfiles("test")
@ImportAutoConfiguration(classes = { JacksonAutoConfiguration.class, ValidationAutoConfiguration.class })
class NoopProcessingEventAsyncPublisherTest
{
	@Autowired
	private NoopProcessingEventAsyncPublisher publisher;

	@Autowired
	private ProcessingEvent event;

	@Test
	void testPublishEventWithNullEvent()
	{
		final RuntimeException actual = Assertions.assertThrows(RuntimeException.class, () -> {
			this.publisher.publishEvent(null);
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testPublishEventAlwaysSuccessful()
	{
		StepVerifier.create(this.publisher.publishEvent(this.event))
				.expectNextMatches(
						aResult -> aResult.getEventId() != null
								&& aResult.isSuccessful())
				.expectComplete()
				.verify(Duration.ofSeconds(15));
	}

}
