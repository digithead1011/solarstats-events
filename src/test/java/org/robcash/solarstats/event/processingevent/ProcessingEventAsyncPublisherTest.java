/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent;

import java.time.Duration;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.robcash.solarstats.event.PublishResult;
import org.robcash.solarstats.event.config.EventsConfigProperties;
import org.robcash.solarstats.event.processingevent.config.ProcessingEventAsyncPublisherTestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import software.amazon.awssdk.awscore.exception.AwsErrorDetails;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.services.eventbridge.EventBridgeAsyncClient;
import software.amazon.awssdk.services.eventbridge.model.EventBridgeException;
import software.amazon.awssdk.services.eventbridge.model.PutEventsResponse;
import software.amazon.awssdk.services.eventbridge.model.PutEventsResultEntry;

/**
 * Unit tests for {@link ProcessingEventPublisher}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootTest(classes = ProcessingEventAsyncPublisherTestConfig.class)
@ActiveProfiles("test")
@ImportAutoConfiguration(classes = { JacksonAutoConfiguration.class, ValidationAutoConfiguration.class })
class ProcessingEventAsyncPublisherTest
{
	@Autowired
	@Qualifier("mockEBAsyncClient")
	private EventBridgeAsyncClient client;

	@Autowired
	private EventsConfigProperties config;

	@Autowired
	private ProcessingEventAsyncPublisher publisher;

	@Autowired
	@Qualifier("statsRetrievalStartedEvent")
	private ProcessingEvent retrievalStartedEvent;

	@Test
	void testConstructWithNullClient()
	{
		final RuntimeException actual = Assertions.assertThrows(RuntimeException.class, () -> {
			new ProcessingEventAsyncPublisher(null, this.config.getProcessingEvents());
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testConstructWithNullConfig()
	{
		final RuntimeException actual = Assertions.assertThrows(RuntimeException.class, () -> {
			new ProcessingEventAsyncPublisher(this.client, null);
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testPublishNullEvent()
	{
		final RuntimeException actual = Assertions.assertThrows(RuntimeException.class, () -> {
			this.publisher.publishEvent(null);
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testPublishOfInvalidEvent()
	{
		final ProcessingError error = ProcessingError.builder().build();
		final ProcessingEvent event = ProcessingEvent.builder()
				.withVersion(null)
				.withPhase(null)
				.withAction(null)
				.withErrorDetails(error)
				.build();
		final ConstraintViolationException actual = Assertions.assertThrows(ConstraintViolationException.class, () -> {
			this.publisher.publishEvent(event);
		});
		final Set<ConstraintViolation<?>> violations = actual.getConstraintViolations();
		Assertions.assertNotNull(violations, "Constraint violations should not be null");
		Assertions.assertTrue(violations.stream()
				.anyMatch(v -> "{validation.processingevent.version.required}".equals(v.getMessageTemplate())));
		Assertions.assertTrue(violations.stream()
				.anyMatch(v -> "{validation.processingevent.action.required}".equals(v.getMessageTemplate())));
		Assertions.assertTrue(violations.stream()
				.anyMatch(v -> "{validation.processingevent.phase.required}".equals(v.getMessageTemplate())));
		Assertions.assertTrue(violations.stream()
				.anyMatch(v -> "{validation.processingevent.error.severity.required}".equals(v.getMessageTemplate())));
		Assertions.assertTrue(violations.stream()
				.anyMatch(
						v -> "{validation.processingevent.error.message.required}".equals(v.getMessageTemplate())));
	}

	@SuppressWarnings("unchecked")
	@Test
	void testPublishWithoutEventBridgeErrors()
	{
		final PutEventsResultEntry resultEntry = PutEventsResultEntry.builder()
				.eventId("successful-test-event-id")
				.build();

		final PutEventsResponse ebResponse = PutEventsResponse.builder()
				.entries(Collections.singleton(resultEntry))
				.build();

		final CompletableFuture<PutEventsResponse> promise = new CompletableFuture<>();
		promise.complete(ebResponse);
		Mockito.when(this.client.putEvents(Mockito.any(Consumer.class))).thenReturn(promise);

		final Mono<PublishResult> result = this.publisher.publishEvent(this.retrievalStartedEvent);
		Assertions.assertNotNull(result, "Result should not be null");
		StepVerifier.create(result)
				.expectNextMatches(
						aResult -> resultEntry.eventId().equals(aResult.getEventId())
								&& aResult.isSuccessful())
				.expectComplete()
				.verify(Duration.ofSeconds(15));
	}

	@SuppressWarnings("unchecked")
	@Test
	void testPublishWithEventBridgeError()
	{
		final PutEventsResultEntry resultEntry = PutEventsResultEntry.builder()
				.errorCode("MockError")
				.errorMessage("Mock error")
				.build();

		final PutEventsResponse ebResponse = PutEventsResponse.builder()
				.entries(Collections.singleton(resultEntry))
				.build();

		final CompletableFuture<PutEventsResponse> promise = new CompletableFuture<>();
		promise.complete(ebResponse);
		Mockito.when(this.client.putEvents(Mockito.any(Consumer.class))).thenReturn(promise);

		final Mono<PublishResult> result = this.publisher.publishEvent(this.retrievalStartedEvent);
		Assertions.assertNotNull(result, "Result should not be null");
		StepVerifier.create(result)
				.expectNextMatches(
						aResult -> resultEntry.errorCode().equals(aResult.getErrorCode())
								&& resultEntry.errorMessage().equals(aResult.getErrorMessage())
								&& !aResult.isSuccessful())
				.expectComplete()
				.verify(Duration.ofSeconds(15));
	}

	@SuppressWarnings("unchecked")
	@Test
	void testPublishWithEventBridgeException()
	{
		final AwsServiceException ex = EventBridgeException.builder()
				.awsErrorDetails(AwsErrorDetails.builder().errorCode("foo").errorMessage("error").build())
				.build();
		final CompletableFuture<PutEventsResponse> promise = new CompletableFuture<>();
		promise.completeExceptionally(ex);
		Mockito.when(this.client.putEvents(Mockito.any(Consumer.class))).thenReturn(promise);

		final Mono<PublishResult> result = this.publisher.publishEvent(this.retrievalStartedEvent);
		Assertions.assertNotNull(result, "Result should not be null");
		StepVerifier.create(result)
				.expectError()
				.verify(Duration.ofSeconds(15));
	}

}
