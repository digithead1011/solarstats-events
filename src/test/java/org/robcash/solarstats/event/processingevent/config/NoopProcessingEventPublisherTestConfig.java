/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent.config;

import org.robcash.solarstats.event.config.EventsConfigProperties;
import org.robcash.solarstats.event.processingevent.NoopProcessingEventAsyncPublisher;
import org.robcash.solarstats.event.processingevent.NoopProcessingEventPublisher;
import org.robcash.solarstats.event.processingevent.ProcessingEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Spring config for {@link ProcessingEventPublisherTest}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(EventsConfigProperties.class)
@Profile("test")
public class NoopProcessingEventPublisherTestConfig
{
	@Bean
	NoopProcessingEventPublisher noopPublisher()
	{
		return new NoopProcessingEventPublisher();
	}

	@Bean
	NoopProcessingEventAsyncPublisher noopAsyncPublisher()
	{
		return new NoopProcessingEventAsyncPublisher();
	}

	@Bean
	ProcessingEvent statsRetrievalStartedEvent()
	{
		return ProcessingEvent.newStatsRetrievalStartedEvent();
	}
}
