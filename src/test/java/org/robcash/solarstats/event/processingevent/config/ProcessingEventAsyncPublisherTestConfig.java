/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent.config;

import org.mockito.Mockito;
import org.robcash.solarstats.event.config.EventsConfigProperties;
import org.robcash.solarstats.event.processingevent.ProcessingEventAsyncPublisher;
import org.robcash.solarstats.event.processingevent.ProcessingEvent;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.services.eventbridge.EventBridgeAsyncClient;

/**
 * Spring config for {@link ProcessingEventPublisherTest}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(EventsConfigProperties.class)
@Profile("test")
public class ProcessingEventAsyncPublisherTestConfig
{
	@Bean
	EventBridgeAsyncClient mockEBAsyncClient()
	{
		return Mockito.mock(EventBridgeAsyncClient.class);
	}

	@Bean
	EventBridgeAsyncClient actualEBAsyncClient()
	{
		return EventBridgeAsyncClient.builder()
				.credentialsProvider(ProfileCredentialsProvider.create("uploader"))
				.build();
	}

	@Bean
	ProcessingEventAsyncPublisher mockPublisher(@Qualifier("mockEBAsyncClient") final EventBridgeAsyncClient argClient,
			final EventsConfigProperties argConfig)
	{
		return new ProcessingEventAsyncPublisher(argClient, argConfig.getProcessingEvents());
	}

	@Bean
	ProcessingEvent statsRetrievalStartedEvent()
	{
		return ProcessingEvent.newStatsRetrievalStartedEvent();
	}
}
