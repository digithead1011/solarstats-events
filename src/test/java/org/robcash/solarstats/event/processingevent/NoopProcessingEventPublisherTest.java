/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.robcash.solarstats.event.PublishResult;
import org.robcash.solarstats.event.processingevent.config.NoopProcessingEventPublisherTestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/**
 * Unit tests for {@link NoopProcessingEventPublisher}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootTest(classes = NoopProcessingEventPublisherTestConfig.class)
@ActiveProfiles("test")
@ImportAutoConfiguration(classes = { JacksonAutoConfiguration.class, ValidationAutoConfiguration.class })
class NoopProcessingEventPublisherTest
{
	@Autowired
	private NoopProcessingEventPublisher publisher;

	@Autowired
	private ProcessingEvent event;

	@Test
	void testPublishEventWithNullEvent()
	{
		final RuntimeException actual = Assertions.assertThrows(RuntimeException.class, () -> {
			this.publisher.publishEvent(null);
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testPublishEventAlwaysSuccessful()
	{
		final PublishResult result = this.publisher.publishEvent(this.event);
		Assertions.assertNotNull(result, "Publishing result should not be null");
		Assertions.assertTrue(result.isSuccessful(), "Publishing should have been successful");
		Assertions.assertNotNull(result.getEventId(), "Event ID should not be null");
	}

}
