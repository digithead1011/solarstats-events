/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent;

import java.io.IOException;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.robcash.solarstats.event.config.EventDataConfig;
import org.robcash.solarstats.event.processingevent.ProcessingEvent.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Unit tests for {@link ProcessingEvent}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootTest(classes = EventDataConfig.class)
@ActiveProfiles("test")
@ImportAutoConfiguration(classes = { JacksonAutoConfiguration.class })
class ProcessingEventTest
{
	/** Event name. */
	private static final String EVENT_NAME = "SolarStatsProcessingEvent";

	/** Version. */
	private static final String VERSION = "1";

	/** Customer ID. */
	private static final String CUSTOMER_ID = "99999";

	/** Processing error. */
	@Autowired
	private ProcessingError processingError;

	/** Metadata. */
	@Autowired
	@Qualifier("mockMetadata")
	private Map<String, Object> metadata;

	@Autowired
	private ObjectMapper mapper;

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newCustomerRetrievalStartedEvent()}.
	 */
	@Test
	void testNewCustomerRetrievalStartedEvent()
	{
		final ProcessingEvent event = ProcessingEvent.newCustomerRetrievalStartedEvent();
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.CustomerRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Started, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newCustomerRetrievalStartedEvent(java.util.Map)}.
	 */
	@Test
	void testNewCustomerRetrievalStartedEventWithMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newCustomerRetrievalStartedEvent(this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.CustomerRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Started, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newCustomerRetrievalFinishedEvent()}.
	 */
	@Test
	void testNewCustomerRetrievalFinishedEvent()
	{
		final ProcessingEvent event = ProcessingEvent.newCustomerRetrievalFinishedEvent();
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.CustomerRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Finished, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newCustomerRetrievalFinishedEvent(java.util.Map)}.
	 */
	@Test
	void testNewCustomerRetrievalFinishedEventWithMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newCustomerRetrievalFinishedEvent(this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.CustomerRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Finished, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newCustomerRetrievalError(org.robcash.solarstats.event.processingevent.ProcessingError)}.
	 */
	@Test
	void testNewCustomerRetrievalErrorEvent()
	{
		final ProcessingEvent event = ProcessingEvent.newCustomerRetrievalError(this.processingError);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.CustomerRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.ErrorOccurred, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newCustomerRetrievalFinishedEvent(org.robcash.solarstats.event.processingevent.ProcessingError, java.util.Map)}.
	 */
	@Test
	void testNewCustomerRetrievalErrorWithMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newCustomerRetrievalError(this.processingError, this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.CustomerRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.ErrorOccurred, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsRetrievalStartedEvent()}.
	 */
	@Test
	void testNewStatsRetrievalStartedEvent()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsRetrievalStartedEvent();
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Started, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsRetrievalStartedEvent(java.util.Map)}.
	 */
	@Test
	void testNewStatsRetrievalStartedEventWithMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsRetrievalStartedEvent(this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Started, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsRetrievalStartedEvent(java.lang.String)}.
	 */
	@Test
	void testNewStatsRetrievalStartedEventWithCustomerId()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsRetrievalStartedEvent(CUSTOMER_ID);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Started, event.getAction(), "Action is wrong");
		Assertions.assertEquals(CUSTOMER_ID, event.getCustomerId(), "Customer ID is wrong");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsRetrievalStartedEvent(java.lang.String, java.util.Map)}.
	 */
	@Test
	void testNewStatsRetrievalStartedEventWithCustomerIdAndMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsRetrievalStartedEvent(CUSTOMER_ID, this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Started, event.getAction(), "Action is wrong");
		Assertions.assertEquals(CUSTOMER_ID, event.getCustomerId(), "Customer ID is wrong");
		Assertions.assertNotNull(event.getMetadata());
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsRetrievalFinishedEvent()}.
	 */
	@Test
	void testNewStatsRetrievalFinishedEvent()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsRetrievalFinishedEvent();
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Finished, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsRetrievalFinishedEvent(java.util.Map)}.
	 */
	@Test
	void testNewStatsRetrievalFinishedEventWithMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsRetrievalFinishedEvent(this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Finished, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsRetrievalFinishedEvent(java.lang.String)}.
	 */
	@Test
	void testNewStatsRetrievalFinishedEventWithCustomerId()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsRetrievalFinishedEvent(CUSTOMER_ID);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Finished, event.getAction(), "Action is wrong");
		Assertions.assertEquals(CUSTOMER_ID, event.getCustomerId(), "Customer ID is wrong");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsRetrievalFinishedEvent(java.lang.String, java.util.Map)}.
	 */
	@Test
	void testNewStatsRetrievalFinishedEventWithCustomerIdAndMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsRetrievalFinishedEvent(CUSTOMER_ID, this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Finished, event.getAction(), "Action is wrong");
		Assertions.assertEquals(CUSTOMER_ID, event.getCustomerId(), "Customer ID is wrong");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsRetrievalError(org.robcash.solarstats.event.processingevent.ProcessingError)}.
	 */
	@Test
	void testNewStatsRetrievalError()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsRetrievalError(this.processingError);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.ErrorOccurred, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsRetrievalError(org.robcash.solarstats.event.processingevent.ProcessingError, java.util.Map)}.
	 */
	@Test
	void testNewStatsRetrievalErrorWithMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsRetrievalError(this.processingError, this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.ErrorOccurred, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsRetrievalError(org.robcash.solarstats.event.processingevent.ProcessingError, java.lang.String)}.
	 */
	@Test
	void testNewStatsRetrievalErrorWithCustomerId()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsRetrievalError(this.processingError, CUSTOMER_ID);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.ErrorOccurred, event.getAction(), "Action is wrong");
		Assertions.assertEquals(CUSTOMER_ID, event.getCustomerId(), "Customer ID is wrong");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsRetrievalError(org.robcash.solarstats.event.processingevent.ProcessingError, java.lang.String, java.util.Map)}.
	 */
	@Test
	void testNewStatsRetrievalErrorWithCustomerIdAndMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsRetrievalError(this.processingError, CUSTOMER_ID,
				this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsRetrieval, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.ErrorOccurred, event.getAction(), "Action is wrong");
		Assertions.assertEquals(CUSTOMER_ID, event.getCustomerId(), "Customer ID is wrong");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsPublishingStartedEvent()}.
	 */
	@Test
	void testNewStatsPublishingStartedEvent()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsPublishingStartedEvent();
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsPublishing, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Started, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsPublishingStartedEvent(java.util.Map)}.
	 */
	@Test
	void testNewStatsPublishingStartedEventWithMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsPublishingStartedEvent(this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsPublishing, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Started, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsPublishingStartedEvent(java.lang.String)}.
	 */
	@Test
	void testNewStatsPublishingStartedEventWithCustomerId()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsPublishingStartedEvent(CUSTOMER_ID);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsPublishing, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Started, event.getAction(), "Action is wrong");
		Assertions.assertEquals(CUSTOMER_ID, event.getCustomerId(), "Customer ID is wrong");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsPublishingStartedEvent(java.lang.String, java.util.Map)}.
	 */
	@Test
	void testNewStatsPublishingStartedEventWithCustomerIdAndMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsPublishingStartedEvent(CUSTOMER_ID, this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsPublishing, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Started, event.getAction(), "Action is wrong");
		Assertions.assertEquals(CUSTOMER_ID, event.getCustomerId(), "Customer ID is wrong");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsPublishingFinishedEvent()}.
	 */
	@Test
	void testNewStatsPublishingFinishedEvent()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsPublishingFinishedEvent();
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsPublishing, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Finished, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsPublishingFinishedEvent(java.util.Map)}.
	 */
	@Test
	void testNewStatsPublishingFinishedEventWithMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsPublishingFinishedEvent(this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsPublishing, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Finished, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsPublishingFinishedEvent(java.lang.String)}.
	 */
	@Test
	void testNewStatsPublishingFinishedEventWithCustomerId()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsPublishingFinishedEvent(CUSTOMER_ID);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsPublishing, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Finished, event.getAction(), "Action is wrong");
		Assertions.assertEquals(CUSTOMER_ID, event.getCustomerId(), "Customer ID is wrong");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsPublishingFinishedEvent(java.lang.String, java.util.Map)}.
	 */
	@Test
	void testNewStatsPublishingFinishedEventWithCustomerIdAndMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsPublishingFinishedEvent(CUSTOMER_ID, this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsPublishing, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.Finished, event.getAction(), "Action is wrong");
		Assertions.assertEquals(CUSTOMER_ID, event.getCustomerId(), "Customer ID is wrong");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsPublishingError(org.robcash.solarstats.event.processingevent.ProcessingError)}.
	 */
	@Test
	void testNewStatsPublishingError()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsPublishingError(this.processingError);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsPublishing, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.ErrorOccurred, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsPublishingError(org.robcash.solarstats.event.processingevent.ProcessingError, java.util.Map)}.
	 */
	@Test
	void testNewStatsPublishingErrorWithMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsPublishingError(this.processingError, this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsPublishing, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.ErrorOccurred, event.getAction(), "Action is wrong");
		Assertions.assertNull(event.getCustomerId(), "Customer ID should be null");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsPublishingError(org.robcash.solarstats.event.processingevent.ProcessingError, java.lang.String)}.
	 */
	@Test
	void testNewStatsPublishingErrorWithCustomerId()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsPublishingError(this.processingError, CUSTOMER_ID);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsPublishing, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.ErrorOccurred, event.getAction(), "Action is wrong");
		Assertions.assertEquals(CUSTOMER_ID, event.getCustomerId(), "Customer ID is wrong");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	/**
	 * Test method for
	 * {@link org.robcash.solarstats.event.processingevent.ProcessingEvent#newStatsPublishingError(org.robcash.solarstats.event.processingevent.ProcessingError, java.lang.String, java.util.Map)}.
	 */
	@Test
	void testNewStatsPublishingErrorWithCustomerIdAndMetadata()
	{
		final ProcessingEvent event = ProcessingEvent.newStatsPublishingError(this.processingError, CUSTOMER_ID,
				this.metadata);
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(ProcessingPhase.StatsPublishing, event.getPhase(), "Phase is wrong");
		Assertions.assertEquals(ProcessingEvent.Action.ErrorOccurred, event.getAction(), "Action is wrong");
		Assertions.assertEquals(CUSTOMER_ID, event.getCustomerId(), "Customer ID is wrong");
		Assertions.assertNotNull(event.getMetadata(), "Metadata should not be null");
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	@Test
	void testDeserialization() throws IOException
	{
		final ProcessingEvent event = ProcessingEvent.builder()
				.withPhase(ProcessingPhase.StatsRetrieval)
				.withCorrelationId("correlation-id")
				.withAction(Action.Started)
				.withCustomerId(CUSTOMER_ID)
				.withErrorDetails(this.processingError)
				.withMetadata(this.metadata)
				.build();
		final String json = this.mapper.writeValueAsString(event);

		final ProcessingEvent reconstitutedEvent = this.mapper.readValue(json, ProcessingEvent.class);
		Assertions.assertEquals(event.getEventName(), reconstitutedEvent.getEventName(), "Event name is incorrect");
		Assertions.assertEquals(event.getVersion(), reconstitutedEvent.getVersion(), "Version is incorrect");
		Assertions.assertEquals(event.getId(), reconstitutedEvent.getId(), "ID is incorrect");
		Assertions.assertEquals(event.getCorrelationId(), reconstitutedEvent.getCorrelationId(),
				"Correlation ID is incorrect");
		Assertions.assertEquals(event.getPhase(), reconstitutedEvent.getPhase(), "Phase is incorrect");
		Assertions.assertEquals(event.getAction(), reconstitutedEvent.getAction(), "Action is incorrect");
		Assertions.assertEquals(event.getCustomerId(), reconstitutedEvent.getCustomerId(), "Customer ID is incorrect");
		Assertions.assertEquals(event.getErrorDetails(), reconstitutedEvent.getErrorDetails(),
				"Error details is incorrect");
		Assertions.assertEquals(event.getMetadata(), reconstitutedEvent.getMetadata(), "Metadata is incorrect");
	}
}
