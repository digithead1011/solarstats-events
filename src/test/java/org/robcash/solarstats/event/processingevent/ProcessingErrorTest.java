/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.processingevent;

import java.io.IOException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.robcash.solarstats.event.config.EventDataConfig;
import org.robcash.solarstats.event.processingevent.ProcessingError.Severity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Unit tests for {@link ProcessingEvent}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootTest(classes = EventDataConfig.class)
@ActiveProfiles("test")
@ImportAutoConfiguration(classes = { JacksonAutoConfiguration.class })
class ProcessingErrorTest
{
	/** Message. */
	private final String message = "Uh oh.";

	@Autowired
	private ObjectMapper mapper;

	/**
	 * Test method for {@link ProcessingEvent#newWarning()}.
	 */
	@Test
	void testNewWarning()
	{
		final ProcessingError error = ProcessingError.newWarning();
		Assertions.assertEquals(ProcessingError.Severity.Warning, error.getSeverity(), "Severity is wrong");
		Assertions.assertNull(error.getMessage(), "Message should be null");
	}

	/**
	 * Test method for {@link ProcessingEvent#newWarning(String)}.
	 */
	@Test
	void testNewWarningWithMessage()
	{
		final ProcessingError error = ProcessingError.newWarning(this.message);
		Assertions.assertEquals(ProcessingError.Severity.Warning, error.getSeverity(), "Severity is wrong");
		Assertions.assertEquals(this.message, error.getMessage(), "Message is wrong");
	}

	/**
	 * Test method for {@link ProcessingEvent#newError()}.
	 */
	@Test
	void testNewError()
	{
		final ProcessingError error = ProcessingError.newError();
		Assertions.assertEquals(ProcessingError.Severity.Error, error.getSeverity(), "Severity is wrong");
		Assertions.assertNull(error.getMessage(), "Message should be null");
	}

	/**
	 * Test method for {@link ProcessingEvent#newError(String)}.
	 */
	@Test
	void testNewErrorWithMessage()
	{
		final ProcessingError error = ProcessingError.newError(this.message);
		Assertions.assertEquals(ProcessingError.Severity.Error, error.getSeverity(), "Severity is wrong");
		Assertions.assertEquals(this.message, error.getMessage(), "Message is wrong");
	}

	/**
	 * Test method for {@link ProcessingEvent#newFatal()}.
	 */
	@Test
	void testNewFatal()
	{
		final ProcessingError error = ProcessingError.newFatal();
		Assertions.assertEquals(ProcessingError.Severity.Fatal, error.getSeverity(), "Severity is wrong");
		Assertions.assertNull(error.getMessage(), "Message should be null");
	}

	/**
	 * Test method for {@link ProcessingEvent#newFatal(String)}.
	 */
	@Test
	void testNewFatalWithMessage()
	{
		final ProcessingError error = ProcessingError.newFatal(this.message);
		Assertions.assertEquals(ProcessingError.Severity.Fatal, error.getSeverity(), "Severity is wrong");
		Assertions.assertEquals(this.message, error.getMessage(), "Message is wrong");
	}

	@Test
	void testDeserialization() throws IOException
	{
		final ProcessingError error = ProcessingError.builder()
				.withMessage(this.message)
				.withSeverity(Severity.Error)
				.build();
		final String json = this.mapper.writeValueAsString(error);

		final ProcessingError reconstitutedError = this.mapper.readValue(json, ProcessingError.class);
		Assertions.assertEquals(error.getMessage(), reconstitutedError.getMessage(), "Message is incorrect");
		Assertions.assertEquals(error.getSeverity(), reconstitutedError.getSeverity(), "Severity is incorrect");
	}
}
