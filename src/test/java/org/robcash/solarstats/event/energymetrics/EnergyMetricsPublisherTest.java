/*
 * Copyright (C) 2023-2024 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.energymetrics;

import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.robcash.solarstats.event.PublishResult;
import org.robcash.solarstats.event.config.EventDataConfig;
import org.robcash.solarstats.event.config.EventsConfigProperties;
import org.robcash.solarstats.event.energymetrics.EnergyMetricsEvent.Action;
import org.robcash.solarstats.event.energymetrics.config.EnergyMetricsPublisherTestConfig;
import org.robcash.solarstats.monitoring.model.EnergyMetrics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import software.amazon.awssdk.awscore.exception.AwsErrorDetails;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.services.eventbridge.EventBridgeClient;
import software.amazon.awssdk.services.eventbridge.model.EventBridgeException;
import software.amazon.awssdk.services.eventbridge.model.PutEventsRequest;
import software.amazon.awssdk.services.eventbridge.model.PutEventsResponse;
import software.amazon.awssdk.services.eventbridge.model.PutEventsResultEntry;

/**
 * Unit tests for {@link EnergyMetricsPublisher}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootTest(classes = { EnergyMetricsPublisherTestConfig.class, EventDataConfig.class })
@ActiveProfiles("test")
@ImportAutoConfiguration(classes = { JacksonAutoConfiguration.class, ValidationAutoConfiguration.class })
class EnergyMetricsPublisherTest
{
	@Autowired
	private EventsConfigProperties config;

	@Autowired
	private EventBridgeClient client;

	@Autowired
	@Qualifier("energyMetricsPublisher")
	private EnergyMetricsPublisher publisher;

	@Autowired
	private EnergyMetrics metrics;

	@Test
	void testConstructWithNullClient()
	{
		final RuntimeException actual = Assertions.assertThrows(RuntimeException.class, () -> {
			new EnergyMetricsPublisher(null, this.config.getEnergyMetrics());
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testConstructWithNullC()
	{
		final RuntimeException actual = Assertions.assertThrows(RuntimeException.class, () -> {
			new EnergyMetricsPublisher(this.client, null);
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testPublishWithNullEvent()
	{
		final RuntimeException actual = Assertions.assertThrows(RuntimeException.class, () -> {
			this.publisher.publishEvent(null);
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testPublishInvalidEventWithNullMetrics()
	{
		final EnergyMetricsEvent event = EnergyMetricsEvent.builder()
				.withVersion(null)
				.build();
		final ConstraintViolationException actual = Assertions.assertThrows(ConstraintViolationException.class, () -> {
			this.publisher.publishEvent(event);
		});
		final Set<ConstraintViolation<?>> violations = actual.getConstraintViolations();
		Assertions.assertNotNull(violations, "Constraint violations should not be null");
		Assertions.assertTrue(violations.stream()
				.anyMatch(v -> "{validation.energymetricsevent.version.required}".equals(v.getMessageTemplate())));
		Assertions.assertTrue(violations.stream()
				.anyMatch(v -> "{validation.energymetricsevent.action.required}".equals(v.getMessageTemplate())));
		Assertions.assertTrue(violations.stream()
				.anyMatch(v -> "{validation.energymetricsevent.metrics.required}".equals(v.getMessageTemplate())));
	}

	@Test
	void testPublishWithException()
	{
		final AwsServiceException ex = EventBridgeException.builder()
				.awsErrorDetails(AwsErrorDetails.builder().errorCode("foo").errorMessage("error").build())
				.build();
		Mockito.when(this.client.putEvents(Mockito.any(PutEventsRequest.class))).thenThrow(ex);

		final EnergyMetricsEvent event = EnergyMetricsEvent.builder()
				.withAction(Action.MetricsRetrieved)
				.withCustomerId("999")
				.withMetrics(this.metrics)
				.build();
		final PublishResult result = this.publisher.publishEvent(event);
		Assertions.assertNotNull(result, "Publish result should never be null");
		Assertions.assertFalse(result.isSuccessful(), "Publishing should have failed");
		Assertions.assertNotNull(result.getErrorMessage(), "Error nessage should not be null");
		Assertions.assertNotNull(result.getException(), "Exception should not be null");
	}

	@Test
	void testPublishSuccessful()
	{
		final PutEventsResponse mockResult = PutEventsResponse.builder()
				.entries(PutEventsResultEntry.builder()
						.eventId("test-id").build())
				.build();
		Mockito.when(this.client.putEvents(Mockito.any(PutEventsRequest.class))).thenReturn(mockResult);

		final EnergyMetricsEvent event = EnergyMetricsEvent.builder()
				.withAction(Action.MetricsRetrieved)
				.withCustomerId("999")
				.withMetrics(this.metrics)
				.build();
		final PublishResult result = this.publisher.publishEvent(event);
		Assertions.assertNotNull(result, "Publish result should never be null");
		Assertions.assertTrue(result.isSuccessful(), "Publishing should have succeeded");
		Assertions.assertNotNull(result.getEventId(), "Event ID should not be null");
	}

}
