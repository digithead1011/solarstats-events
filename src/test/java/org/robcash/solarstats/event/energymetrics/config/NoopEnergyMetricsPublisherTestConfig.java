/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.energymetrics.config;

import org.robcash.solarstats.event.config.EventsConfigProperties;
import org.robcash.solarstats.event.energymetrics.NoopEnergyMetricsAsyncPublisher;
import org.robcash.solarstats.event.energymetrics.NoopEnergyMetricsPublisher;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * Spring configuration for several EnergyMetricsPublisher unit test classes.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(EventsConfigProperties.class)
@Profile("test")
public class NoopEnergyMetricsPublisherTestConfig
{
	@Bean
	NoopEnergyMetricsPublisher noopEnergyMetricsPublisher()
	{
		return new NoopEnergyMetricsPublisher();
	}

	@Bean
	NoopEnergyMetricsAsyncPublisher noopAsyncEnergyMetricsPublisher()
	{
		return new NoopEnergyMetricsAsyncPublisher();
	}

	@Bean
	MessageSource eventsValidationMessageSource()
	{
		final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:/org/robcash/solarstats/event/validation");
		return messageSource;
	}

}
