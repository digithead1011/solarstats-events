/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.energymetrics.config;

import org.mockito.Mockito;
import org.robcash.solarstats.event.config.EventsConfigProperties;
import org.robcash.solarstats.event.energymetrics.EnergyMetricsAsyncPublisher;
import org.robcash.solarstats.event.energymetrics.EnergyMetricsPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import software.amazon.awssdk.services.eventbridge.EventBridgeAsyncClient;
import software.amazon.awssdk.services.eventbridge.EventBridgeClient;

/**
 * Spring configuration for several EnergyMetricsPublisher unit test classes.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(EventsConfigProperties.class)
@Profile("test")
public class EnergyMetricsPublisherTestConfig
{
	@Autowired
	private EventsConfigProperties config;

	@Bean
	EventBridgeClient ebClient()
	{
		return Mockito.mock(EventBridgeClient.class);
	}

	@Bean
	EventBridgeAsyncClient asyncEBClient()
	{
		return Mockito.mock(EventBridgeAsyncClient.class);
	}

	@Bean
	EnergyMetricsPublisher energyMetricsPublisher(final EventBridgeClient argClient)
	{
		return new EnergyMetricsPublisher(argClient, this.config.getEnergyMetrics());
	}

	@Bean
	EnergyMetricsAsyncPublisher asyncEnergyMetricsPublisher(final EventBridgeAsyncClient argClient)
	{
		return new EnergyMetricsAsyncPublisher(argClient, this.config.getEnergyMetrics());
	}
}
