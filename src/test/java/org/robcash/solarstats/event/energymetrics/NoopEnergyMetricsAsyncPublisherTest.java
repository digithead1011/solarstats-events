/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.energymetrics;

import java.time.Duration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.robcash.solarstats.event.config.EventDataConfig;
import org.robcash.solarstats.event.energymetrics.EnergyMetricsEvent.Action;
import org.robcash.solarstats.event.energymetrics.config.NoopEnergyMetricsPublisherTestConfig;
import org.robcash.solarstats.monitoring.model.EnergyMetrics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import jakarta.validation.ValidationException;
import reactor.test.StepVerifier;

/**
 * Unit tests for {@link NoopEnergyMetricsAsyncPublisher}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootTest(classes = { NoopEnergyMetricsPublisherTestConfig.class, EventDataConfig.class })
@ActiveProfiles("test")
@ImportAutoConfiguration(classes = { JacksonAutoConfiguration.class, ValidationAutoConfiguration.class })
class NoopEnergyMetricsAsyncPublisherTest
{
	@Autowired
	private NoopEnergyMetricsAsyncPublisher publisher;

	@Autowired
	private EnergyMetrics metrics;

	@Test
	void testPublishEventWithNullEvent()
	{
		final RuntimeException actual = Assertions.assertThrows(RuntimeException.class, () -> {
			this.publisher.publishEvent(null);
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testPublishEventWithNullMetrics()
	{
		final EnergyMetricsEvent event = new EnergyMetricsEvent();
		final ValidationException actual = Assertions.assertThrows(ValidationException.class, () -> {
			this.publisher.publishEvent(event);
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testPublishEventAlwaysSuccessful()
	{
		final EnergyMetricsEvent event = EnergyMetricsEvent.builder()
				.withAction(Action.MetricsRetrieved)
				.withCustomerId("999")
				.withMetrics(this.metrics)
				.build();
		StepVerifier.create(this.publisher.publishEvent(event))
				.expectNextCount(1)
				.expectComplete()
				.verify(Duration.ofSeconds(10));
	}

}
