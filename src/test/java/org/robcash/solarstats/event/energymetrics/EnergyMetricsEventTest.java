/*
 * Copyright (C) 2023-2024 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.energymetrics;

import java.io.IOException;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.robcash.solarstats.event.config.EventDataConfig;
import org.robcash.solarstats.event.energymetrics.EnergyMetricsEvent.Action;
import org.robcash.solarstats.monitoring.model.EnergyMetrics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Unit tests for {@link EnergyMetricsEvent}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootTest(classes = EventDataConfig.class)
@ActiveProfiles("test")
@ImportAutoConfiguration(classes = JacksonAutoConfiguration.class)
class EnergyMetricsEventTest
{
	/** Event. */
	private static final String EVENT_NAME = "SolarStatsMetricsEvent";

	/** Version. */
	private static final String VERSION = "1";

	/** Customer ID. */
	private static final String CUSTOMER_ID = "99999";

	@Autowired
	private EnergyMetrics metrics;

	/** Metadata. */
	@Autowired
	@Qualifier("mockMetadata")
	private Map<String, Object> metadata;

	@Autowired
	private ObjectMapper mapper;

	@Test
	void testStatsRetrievedAction()
	{
		final EnergyMetricsEvent event = EnergyMetricsEvent.builder()
				.withAction(Action.MetricsRetrieved)
				.withMetrics(this.metrics)
				.build();
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(EnergyMetricsEvent.Action.MetricsRetrieved, event.getAction(), "Action is not correct");
		Assertions.assertNotNull(event.getMetrics(), "Metrics should not be null");
		Assertions.assertEquals(1, event.getMetrics().size(), "There should only be 1 set of metrics");
		Assertions.assertEquals(this.metrics, event.getMetrics().get(0), "Metrics is wrong");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	@Test
	void testStatsRetrievedActionWithMetadata()
	{
		final EnergyMetricsEvent event = EnergyMetricsEvent.builder()
				.withAction(Action.MetricsRetrieved)
				.withMetrics(this.metrics)
				.withMetadata(this.metadata)
				.build();
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(EnergyMetricsEvent.Action.MetricsRetrieved, event.getAction(), "Action is not correct");
		Assertions.assertNotNull(event.getMetrics(), "Metrics should not be null");
		Assertions.assertEquals(1, event.getMetrics().size(), "There should only be 1 set of metrics");
		Assertions.assertEquals(this.metrics, event.getMetrics().get(0), "Metrics is wrong");
		Assertions.assertNotNull(event.getMetadata());
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	@Test
	void testStatsPublishedAction()
	{
		final EnergyMetricsEvent event = EnergyMetricsEvent.builder()
				.withAction(Action.MetricsPublished)
				.withMetrics(this.metrics)
				.build();
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(EnergyMetricsEvent.Action.MetricsPublished, event.getAction(), "Action is not correct");
		Assertions.assertNotNull(event.getMetrics(), "Metrics should not be null");
		Assertions.assertEquals(1, event.getMetrics().size(), "There should only be 1 set of metrics");
		Assertions.assertEquals(this.metrics, event.getMetrics().get(0), "Metrics is wrong");
		Assertions.assertNull(event.getMetadata(), "Metadata should be null");
	}

	@Test
	void testStatsPublishedActionWithMetadata()
	{
		final EnergyMetricsEvent event = EnergyMetricsEvent.builder()
				.withAction(Action.MetricsPublished)
				.withMetrics(this.metrics)
				.withMetadata(this.metadata)
				.build();
		Assertions.assertEquals(EVENT_NAME, event.getEventName(), "Event name is wrong");
		Assertions.assertEquals(VERSION, event.getVersion(), "Version is wrong");
		Assertions.assertEquals(EnergyMetricsEvent.Action.MetricsPublished, event.getAction(), "Action is not correct");
		Assertions.assertNotNull(event.getMetrics(), "Metrics should not be null");
		Assertions.assertEquals(1, event.getMetrics().size(), "There should only be 1 set of metrics");
		Assertions.assertEquals(this.metrics, event.getMetrics().get(0), "Metrics is wrong");
		Assertions.assertNotNull(event.getMetadata());
		Assertions.assertFalse(event.getMetadata().isEmpty(), "Metadata should not be empty");
	}

	@Test
	void testSerialization() throws JsonProcessingException
	{
		final EnergyMetricsEvent event = EnergyMetricsEvent.builder()
				.withCorrelationId("correlation-id")
				.withAction(Action.MetricsRetrieved)
				.withCustomerId(CUSTOMER_ID)
				.withMetrics(this.metrics)
				.withMetadata(this.metadata)
				.build();
		Assertions.assertNotNull(this.mapper.writeValueAsString(event), "Serialization failed");
	}

	@Test
	void testDeserialization() throws IOException
	{
		final EnergyMetricsEvent event = EnergyMetricsEvent.builder()
				.withCorrelationId("correlation-id")
				.withAction(Action.MetricsRetrieved)
				.withCustomerId(CUSTOMER_ID)
				.withMetrics(this.metrics)
				.withMetadata(this.metadata)
				.build();
		final String json = this.mapper.writeValueAsString(event);

		final EnergyMetricsEvent reconstitutedEvent = this.mapper.readValue(json, EnergyMetricsEvent.class);
		Assertions.assertEquals(event.getEventName(), reconstitutedEvent.getEventName(), "Event name is incorrect");
		Assertions.assertEquals(event.getVersion(), reconstitutedEvent.getVersion(), "Version is incorrect");
		Assertions.assertEquals(event.getId(), reconstitutedEvent.getId(), "ID is incorrect");
		Assertions.assertEquals(event.getCorrelationId(), reconstitutedEvent.getCorrelationId(),
				"Correlation ID is incorrect");
		Assertions.assertEquals(event.getAction(), reconstitutedEvent.getAction(), "Action is incorrect");
		Assertions.assertEquals(event.getCustomerId(), reconstitutedEvent.getCustomerId(), "Customer ID is incorrect");
		Assertions.assertNotNull(reconstitutedEvent.getMetrics(), "Metrics should never be null");
		Assertions.assertEquals(1, reconstitutedEvent.getMetrics().size(), "Metrics not of expected size");
		Assertions.assertEquals(this.metrics, reconstitutedEvent.getMetrics().get(0),
				"Metrics are incorrect");
		Assertions.assertEquals(event.getMetadata(), reconstitutedEvent.getMetadata(), "Metadata is incorrect");
	}

}
