/*
 * Copyright (C) 2023 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.energymetrics;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.robcash.solarstats.event.config.EventDataConfig;
import org.robcash.solarstats.event.config.EventsConfigProperties;
import org.robcash.solarstats.event.energymetrics.config.EnergyMetricsPublisherTestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import jakarta.validation.ValidationException;
import software.amazon.awssdk.services.eventbridge.EventBridgeAsyncClient;

/**
 * Unit tests for {@link EnergyMetricsAsyncPublisher}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootTest(classes = { EnergyMetricsPublisherTestConfig.class, EventDataConfig.class })
@ActiveProfiles("test")
@ImportAutoConfiguration(classes = { JacksonAutoConfiguration.class, ValidationAutoConfiguration.class })
class EnergyMetricsAsyncPublisherTest
{
	@Autowired
	private EventsConfigProperties config;

	@Autowired
	private EventBridgeAsyncClient client;

	@Autowired
	@Qualifier("asyncEnergyMetricsPublisher")
	private EnergyMetricsAsyncPublisher publisher;

	@Test
	void testConstructWithNullClient()
	{
		final RuntimeException actual = Assertions.assertThrows(RuntimeException.class, () -> {
			new EnergyMetricsAsyncPublisher(null, this.config.getEnergyMetrics());
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testConstructWithNullConfig()
	{
		final RuntimeException actual = Assertions.assertThrows(RuntimeException.class, () -> {
			new EnergyMetricsAsyncPublisher(this.client, null);
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testPublishEventWithNullEvent()
	{
		final RuntimeException actual = Assertions.assertThrows(RuntimeException.class, () -> {
			this.publisher.publishEvent(null);
		});
		Assertions.assertNotNull(actual);
	}

	@Test
	void testPublishEventWithNullMetrics()
	{
		final EnergyMetricsEvent event = new EnergyMetricsEvent();
		final ValidationException actual = Assertions.assertThrows(ValidationException.class, () -> {
			this.publisher.publishEvent(event);
		});
		Assertions.assertNotNull(actual);
	}

}
