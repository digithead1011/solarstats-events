/*
 * Copyright (C) 2023-2024 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.event.config;

import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.robcash.solarstats.event.processingevent.ProcessingError;
import org.robcash.solarstats.event.processingevent.ProcessingError.Severity;
import org.robcash.solarstats.monitoring.model.EnergyConsumption;
import org.robcash.solarstats.monitoring.model.EnergyMetrics;
import org.robcash.solarstats.monitoring.model.EnergyProduction;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Common event data configuration.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
@Profile("test")
public class EventDataConfig
{

	@Bean
	EnergyMetrics energyMetrics(final EnergyProduction productionData, final EnergyConsumption energyUsage)
	{
		return new EnergyMetrics()
				.production(productionData)
				.consumption(energyUsage);
	}

	@Bean
	EnergyProduction productionData()
	{
		return new EnergyProduction()
				.dataSource("TestSource")
				.systemId("test-id")
				.systemName("Test System")
				.wattHours(10000)
				.periodStart("Start Time")
				.periodEnd("End Time")
				.asOfDate(Date.from(Instant.now()));
	}

	@Bean
	EnergyConsumption energyUsage()
	{
		return new EnergyConsumption()
				.dataSource("TestSource")
				.monitorId("Monitor-1")
				.energyFromGrid(15000)
				.energyToGrid(20000)
				.asOfDate(Date.from(Instant.now()));
	}

	@Bean
	Map<String, Object> mockMetadata()
	{
		final Map<String, Object> metadata = new HashMap<>();
		metadata.put("key-for-string1", "value1");
		metadata.put("key-for-string2", "value2");
		metadata.put("key-for-map", Collections.singletonMap("inner-key", "value"));

		return metadata;
	}

	@Bean
	ProcessingError processingError()
	{
		return ProcessingError.builder()
				.withSeverity(Severity.Error)
				.withMessage("Uh-oh")
				.build();
	}
}
